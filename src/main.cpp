/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * main.cpp
 *
 *  Created on: 2012-06-19
 *      Author: Jeremy Carter
 */

#include "../include/main.hpp"

/// the main routine
/** Here we spawn an OpenGL context which opens in an SFML window,
    then we start the main loop routine. */
int main( int argc, 	///< [in] number of command line arguments
		  char** argv )	///< [in] the command line arguments
{
	std::string title("Rocketpack Ruckus v0.0");

	std::cout << title << " is starting..." << std::endl << std::endl;

	std::cout << "License information:" << std::endl
			  << "*  Copyright 2012 Jeremy Carter" << std::endl
			  << "* ------------------------------" << std::endl
			  << "*  Rocketpack Ruckus is free software: you can redistribute it and/or" << std::endl
			  << "*  modify it under the terms of the GNU General Public License version 3," << std::endl
			  << "*  as published by the Free Software Foundation." << std::endl
			  << "*" << std::endl
			  << "*  Rocketpack Ruckus is distributed in the hope that it will be useful," << std::endl
			  << "*  but WITHOUT ANY WARRANTY; without even the implied warranty of" << std::endl
			  << "*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" << std::endl
			  << "*  GNU General Public License for more details." << std::endl
			  << "*" << std::endl
			  << "*  You should have received a copy of the GNU General Public License" << std::endl
			  << "*  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>." << std::endl
			  << "* ------------------------------" << std::endl
			  << std::endl;

	def::Video video_settings("conf/video.conf"); // get the video settings

	def::OpenGL3_Context opengl3_context( // create an OpenGL3 context
			video_settings.width, video_settings.height, video_settings.fullscreen, title );

	def::main_loop(opengl3_context); // call main loop routine

	return 0;
}
