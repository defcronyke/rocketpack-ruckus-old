/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Video.cpp
 *
 *  Created on: 2012-08-27
 *      Author: Jeremy Carter
 */

#include "../include/Video.hpp"

namespace def
{

Video::Video(const std::string& video_conf_file)
	: _video_conf(video_conf_file),
	  width(0),
	  height(0)
{
	std::stringstream ss;

	for (unsigned int i = 0; i < _video_conf.content().length(); i++)
	{
		if (_video_conf.content()[i] != '#')
			ss << _video_conf.content()[i];
		else
		{
			i = _video_conf.content().find('\n', i);
			ss << '\n';
		}
	}

	std::string line;

	while (getline(ss, line))
	{
		if (line != "")
		{
			int colon_pos = line.find(':', 0);
			std::string before_colon(line.substr(0, colon_pos));
			std::string after_colon(line.substr(colon_pos+1, line.length()-colon_pos));
			boost::trim(before_colon);
			boost::trim(after_colon);
			video_conf_map[before_colon] = after_colon;
		}
	}

	_parse_resolution();
}

void Video::_parse_resolution()
{
	bool res_error = false;
	if (video_conf_map.find("Resolution") != video_conf_map.end())
	{
		size_t x = video_conf_map["Resolution"].find('x', 0);
		if (x != std::string::npos)
		{
			std::stringstream width_ss(video_conf_map["Resolution"].substr(0, x));
			std::stringstream height_ss(video_conf_map["Resolution"].substr(x+1, video_conf_map["Resolution"].find('\n', x)));
			width_ss >> width;
			height_ss >> height;
			if (width != 0 && height != 0)
			{
				std::cout << "got screen resolution from video.conf" << std::endl;
			}
			else
			{
				res_error = true;
			}
		}
		else
		{
			res_error = true;
		}
	}
	else
	{
		res_error = true;
	}

	if (video_conf_map.find("Fullscreen") != video_conf_map.end())
	{
		if (video_conf_map["Fullscreen"] == "yes")
			fullscreen = true;
		else if (video_conf_map["Fullscreen"] == "no")
			fullscreen = false;
		else
			res_error = true;
	}
	else
		fullscreen = false;

	if (res_error)
	{
		width = 800;
		height = 600;

		std::cerr << "failed to get screen resolution from video.conf\n" << std::endl
				  << "you should check the contents of conf/video.conf for errors" << std::endl
				  << "falling back to the default resolution" << std::endl;
	}

	std::cout << "resolution width: " << width << std::endl
			  << "resolution height: " << height << std::endl;
}

Video::~Video()
{

}

}
