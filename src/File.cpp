/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * File.cpp
 *
 *  Created on: 2012-06-19
 *      Author: Jeremy Carter
 */

#include "../include/File.hpp"

namespace def
{

File::File(const std::string& filename)
{
	open(filename);
}

File::File()
{

}

File::~File()
{

}

void File::open(const std::string& filename)
{
	_filename = filename;

	// look for file relative to current directory
	_filestream.open(filename.c_str());
	if (!_filestream.is_open())
	{
		// look for file relative to data installation path
		_filestream.open((DATADIR"/"+filename).c_str());
		if (!_filestream.is_open())
		{
			// look for file relative to conf installation path
			// strip conf directory from filename
			std::string filename_nodir(filename.substr(filename.find('/')+1, filename.length()));

			#ifndef COMPILING_FOR_WINDOWS
			_filestream.open(("/etc/"PROJECTNAME"/"+filename_nodir).c_str());


			if (!_filestream.is_open())
			{
			#endif
				std::cerr << "file error: unable to open " << filename_nodir << std::endl;
				throw exception_File();
			#ifndef COMPILING_FOR_WINDOWS
			}
			#endif
		}
	}

	std::string line;
	while (_filestream.good())
	{
		getline(_filestream, line);
		_content += line + '\n';
	}

	_filestream.close();

	std::cout << "file loaded: " << _filename << std::endl;

}

const std::string& File::content()
{
	return _content;
}

}
