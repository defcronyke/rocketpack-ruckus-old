/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * main_loop.cpp
 *
 *  Created on: 2012-07-10
 *      Author: Jeremy Carter
 */

#include "../include/main_loop.hpp"

namespace def
{

/** In here we instantiate the world we're going to use,
 	then we begin looping over the event handling and display
 	routines. */
void main_loop( def::OpenGL3_Context& opengl3_context ) ///< [in] the OpenGL context handler
													  	///<  	  for the main window
{
	// The Lua level loader
	def::Lua_Level_Loader lll(opengl3_context);

//	def::Shader shaders("shaders/default.vert", "shaders/default.frag");

//	def::Test_World test_world(opengl3_context); // instantiate test world
	sf::Clock clock; // the timer

	// begin main loop
	while (opengl3_context.window->isOpen())
	{
		float time = clock.getElapsedTime().asSeconds(); // time since last frame

		static float time_counter;
		time_counter += time;	// make sure we only display FPS once about every 5 seconds

		// output framerate
		if (time_counter >= 5)
		{
			std::cout << "FPS: " << 1 / time << std::endl;
			time_counter = 0.0f;
		}

		clock.restart(); // restart the timer

//		test_world.display(); 		// render scene
//		test_world.display_hud();	// render Heads-Up Display
		lll.display();

		sf::Event event;	// non-real-time event object
		def::handle_main_events(opengl3_context, event); // handle system events

		lll.handle_events(time);
		//		test_world.handle_events(time);	// world-specific real-time event handler

		opengl3_context.window->display(); // flush display buffer to screen
	}
	// end main loop
}

}
