/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * RR_Pause_Menu.cpp
 *
 *  Created on: 2012-10-05
 *      Author: Jeremy Carter
 */

#include "../include/RR_Pause_Menu.hpp"

namespace def
{

RR_Pause_Menu::RR_Pause_Menu(def::OpenGL3_Context& opengl3_context)
	: _pm_shaders("shaders/pause_menu.vert", "shaders/pause_menu.frag"),
	  _pm_vao_buffer( new GLuint[1] ),
	  _pm_mvp_loc( new GLint[1] ),
	  _pm_tex_loc( new GLint[1] ),
	  _pm_tex_buffer( new GLuint[1] ),
	  _pm_mvp( glm::ortho(0.0f, (GLfloat)opengl3_context.window_width, 0.0f, (GLfloat)opengl3_context.window_height, -1.0f, 1.0f) ),
	  _pm_vertices_buffer( new GLuint[1] ),
	  _pm_vertices( new GLfloat[12] ),
	  _pm_colors_buffer( new GLuint[1] ),
	  _pm_colors( new GLfloat[16] ),
	  _pm_tex_coords_buffer( new GLuint[1] ),
	  _pm_tex_coords( new GLfloat[8] ),
	  _pm_normals_buffer( new GLuint[1] ),
	  _pm_normals( new GLfloat[12] ),
	  _pm_first_render_error( true )
{
	pm_init(opengl3_context.window_width, opengl3_context.window_height);
}

RR_Pause_Menu::~RR_Pause_Menu()
{

}

/** Initialize the Pause Menu */
void RR_Pause_Menu::pm_init(int resolution_width, int resolution_height)
{
	glGetError(); // clear error status so we can check if error occured inside
				  // this function
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	// set alpha blending mode

	_pm_image.loadFromFile("resources/pause_menu/pause_menu.png");
	_pm_image.flipVertically();

	unsigned int pm_width = _pm_image.getSize().x;
	unsigned int pm_height = _pm_image.getSize().y;

	_pm_vertices[0] = 0.0f; _pm_vertices[1] = resolution_height; _pm_vertices[2] = 0.0f;
	_pm_vertices[3] = 0.0f; _pm_vertices[4] = resolution_height - resolution_height; _pm_vertices[5] = 0.0f;
	_pm_vertices[6] = 0.0f + resolution_width; _pm_vertices[7] = resolution_height - resolution_height; _pm_vertices[8] = 0.0f;
	_pm_vertices[9] = 0.0f + resolution_width; _pm_vertices[10] = resolution_height; _pm_vertices[11] = 0.0f;

	_pm_colors[0] = 1.0f; _pm_colors[1] = 1.0f; _pm_colors[2] = 1.0f; _pm_colors[3] = 0.0f;
	_pm_colors[4] = 1.0f; _pm_colors[5] = 1.0f; _pm_colors[6] = 1.0f; _pm_colors[7] = 0.0f;
	_pm_colors[8] = 1.0f; _pm_colors[9] = 1.0f; _pm_colors[10] = 1.0f; _pm_colors[11] = 0.0f;
	_pm_colors[12] = 1.0f; _pm_colors[13] = 1.0f; _pm_colors[14] = 1.0f; _pm_colors[15] = 0.0f;

	_pm_tex_coords[0] = 0.0f; _pm_tex_coords[1] = pm_height;
	_pm_tex_coords[2] = 0.0f; _pm_tex_coords[3] = 0.0f;
	_pm_tex_coords[4] = pm_width; _pm_tex_coords[5] = 0.0f;
	_pm_tex_coords[6] = pm_width; _pm_tex_coords[7] = pm_height;

	_pm_normals[0] = 0.0f; _pm_normals[1] = 0.0f; _pm_normals[2] = 0.0f;
	_pm_normals[3] = 0.0f; _pm_normals[4] = 0.0f; _pm_normals[5] = 0.0f;
	_pm_normals[6] = 0.0f; _pm_normals[7] = 0.0f; _pm_normals[8] = 0.0f;
	_pm_normals[9] = 0.0f; _pm_normals[10] = 0.0f; _pm_normals[11] = 0.0f;

	glGenVertexArrays(1, &_pm_vao_buffer[0]); // generate VAO
	glBindVertexArray(_pm_vao_buffer[0]);	   // bind VAO

	// vertices
	glEnableVertexAttribArray((GLuint)0);
	glGenBuffers(1, &_pm_vertices_buffer[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _pm_vertices_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), _pm_vertices.get(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normals (inexplicably needed even though it won't be used in the shaders)
	glEnableVertexAttribArray((GLuint)1);
	glGenBuffers(1, &_pm_normals_buffer[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _pm_normals_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), _pm_normals.get(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// texture
	glEnableVertexAttribArray((GLuint)2);
	glGenBuffers(1, &_pm_tex_coords_buffer[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _pm_tex_coords_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), _pm_tex_coords.get(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenTextures(1, &_pm_tex_buffer[0]);
	glBindTexture(GL_TEXTURE_RECTANGLE, _pm_tex_buffer[0]);

	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA, pm_width, pm_height,
			0, GL_RGBA, GL_UNSIGNED_BYTE, _pm_image.getPixelsPtr());
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	// colors
	glEnableVertexAttribArray((GLuint)3);
	glGenBuffers(1, &_pm_colors_buffer[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _pm_colors_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), _pm_colors.get(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)3, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0); // unbind buffers

	_pm_mvp_loc[0] = glGetUniformLocation(_pm_shaders.id(), "mvpMatrix");
	_pm_tex_loc[0] = glGetUniformLocation(_pm_shaders.id(), "pauseMenuTex");

	// check for OpenGL errors
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
		std::cerr << "GL Error in Lua_Level_Loader::pm_init(): "
				  << std::hex << error << std::endl;

	return;
}

void RR_Pause_Menu::pm_handle_events(float& time)
{
	return;
}

void RR_Pause_Menu::pm_display()
{
	glGetError(); // clear error status so we can check if error occured inside
				  // this function
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	_pm_shaders.bind();

		glUniformMatrix4fv(_pm_mvp_loc[0], 1, GL_FALSE, &_pm_mvp[0][0]);
		glUniform1i(_pm_tex_loc[0], 0);

		glBindTexture(GL_TEXTURE_RECTANGLE, _pm_tex_buffer[0]);

//		// pinpoint OpenGL errors
//		GLenum error = glGetError();
//		if (error != GL_NO_ERROR) std::cerr << "GL Error: " << std::hex << error << std::endl;
//		else std::cout << "GL Error: no error" << std::endl;

		glBindVertexArray(_pm_vao_buffer[0]);	 // bind the VAO

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4); // draw the contents of the bound VAO

		glBindVertexArray(0); // unbind the VAO

		glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	_pm_shaders.unbind();

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	// check for OpenGL errors
	GLenum error = glGetError();
	if (error != GL_NO_ERROR && _pm_first_render_error)
	{
		std::cerr << "GL Error in Lua_Level_Loader::pm_display(): "
				  << std::hex << error << std::endl;
		_pm_first_render_error = false;
	}

	return;
}

}
