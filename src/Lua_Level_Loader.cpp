/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Lua_Level_Loader.cpp
 *
 *  Created on: 2012-09-17
 *      Author: Jeremy Carter
 */

#include "../include/Lua_Level_Loader.hpp"

namespace def
{

Lua_Level_Loader::Lua_Level_Loader( def::OpenGL3_Context& opengl3_context,
									unsigned int levelnumber,
									std::string levelpack )
	: _opengl3_context(opengl3_context),
	  _L(luaL_newstate()),
	  _sandboxed_L(luaL_newstate()),
	  _levelnumber(levelnumber),
	  _levelpack(levelpack),
 	  _resolution_width(opengl3_context.window_width),
	  _resolution_height(opengl3_context.window_height),
	  _controls("conf/controls.conf"),
	  _main_character_height(5.0f),
	  _initial_fov(45.0f),
	  _fov(_initial_fov),
	  _shaders("shaders/default.vert", "shaders/default.frag"),
	  _projection_matrix_location(glGetUniformLocation(_shaders.id(), "projectionMatrix")),
	  _view_matrix_location(glGetUniformLocation(_shaders.id(), "viewMatrix")),
	  _model_matrix_location(glGetUniformLocation(_shaders.id(), "modelMatrix")),
	  _modelview_matrix_location(glGetUniformLocation(_shaders.id(), "modelviewMatrix")),
	  _modelviewprojection_matrix_location(glGetUniformLocation(_shaders.id(), "modelviewprojectionMatrix")),
	  _normal_matrix_location(glGetUniformLocation(_shaders.id(), "normalMatrix")),
	  _global_ambient_location(glGetUniformLocation(_shaders.id(), "globalAmbient")),
	  _light_ambient_location(glGetUniformLocation(_shaders.id(), "lightAmbient")),
	  _light_diffuse_location(glGetUniformLocation(_shaders.id(), "lightDiffuse")),
	  _light_specular_location(glGetUniformLocation(_shaders.id(), "lightSpecular")),
	  _light_position_location(glGetUniformLocation(_shaders.id(), "lightPosition")),
	  _texture_sampler_location(glGetUniformLocation(_shaders.id(), "textureSampler")),
	  _projection_matrix( glm::perspective(_fov, (GLfloat)_resolution_width / (GLfloat)_resolution_height, 0.1f, 500.0f) ),
	  _view_matrix( glm::lookAt( glm::vec3(0, _main_character_height, 5), glm::vec3(0, _main_character_height, 0), glm::vec3(0, 1, 0) ) ),
	  _model_matrix( glm::mat4(1.0f) ),
	  _modelview_matrix( _view_matrix * _model_matrix ),
	  _modelviewprojection_matrix( _projection_matrix * _modelview_matrix ),
	  _normal_matrix( glm::transpose(glm::inverse(_view_matrix * _model_matrix)) ),
	  _global_ambient( 0.1f, 0.1f, 0.1f, 1.0f ),
	  _light_ambient( 0.5f, 0.5f, 0.5f, 1.0f ),
	  _light_diffuse( 0.5f, 0.5f, 0.5f, 1.0f ),
	  _light_specular( 0.5f, 0.5f, 0.5f, 1.0f ),
	  _light_position( 100.0f, 100.0f, 100.0f, 0.0f ),
	  _paused(false),
	  _mouse_x_pos(_resolution_width/2),
	  _mouse_y_pos(_resolution_height/2),
	  _mouse_last_x_pos(_mouse_x_pos),
	  _mouse_last_y_pos(_mouse_y_pos),
	  _position( glm::vec3(0.0f, _main_character_height, 20.0f) ),
	  _direction( cos(_vertical_angle) * sin(_horizontal_angle),
			 	  sin(_vertical_angle),
			 	  cos(_vertical_angle) * cos(_horizontal_angle) ),
	  _right( sin(_horizontal_angle - 3.14f/2.0f),
			  0,
			  cos(_horizontal_angle - 3.14f/2.0f) ),
	  _up( glm::cross( _right, _direction ) ),
	  _horizontal_angle(3.14f),
	  _vertical_angle(0.0f),
	  _speed(25.0f),
	  _mouse_speed(0.1f),
	  _window( opengl3_context.window.get() ),
	  _middle_of_window(_resolution_width/2, _resolution_height/2),
	  _jumping(false),
	  _stopped_jumping(true),
	  _rocketpack_is_on(false),
	  _jump_speed(2.4f),
	  _jump_height(20.0f),
	  _jump_hang_time(1.9f),
	  _rocketpack_speed(150.0f),
	  _rocketpack_up_speed(0.3f),
	  _rocketpack_toggle_counter(0),
	  _standing(true),
	  _distance_from_ground(0.0f),
	  _global_gravity(1.88f),
	  pause_menu(opengl3_context),
	  hud(opengl3_context)
{
	luaL_openlibs(_L);
	luaL_openlibs(_sandboxed_L);

	sf::Mouse::setPosition(_middle_of_window, *_window);

	// set background colour
	glClearColor(0.4f, 0.6f, 0.9f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	load_level(opengl3_context, levelnumber, levelpack);
}

Lua_Level_Loader::~Lua_Level_Loader()
{

}

std::string Lua_Level_Loader::_get_Lua_sandboxing_script()
{
	std::string Lua_function_whitelist(
		"assert = assert,"
		"error = error,"
		"ipairs = ipairs,"
		"next = next,"
		"pairs = pairs,"
		"pcall = pcall,"
		"print = print,"
		"select = select,"
		"tonumber = tonumber,"
		"tostring = tostring,"
		"type = type,"
		"unpack = unpack,"
		"_VERSION = _VERSION,"
		"xpcall = xpcall,"
		"coroutine = { create = coroutine.create,"
		"			  resume = coroutine.resume,"
		"			  running = coroutine.running,"
		"			  status = coroutine.status,"
		"			  wrap = coroutine.wrap,"
		"			  yield = coroutine.yield },"
		"math = { abs = math.abs,"
		"	 acos = math.acos,"
		"	 asin = math.asin,"
		"	 atan = math.atan,"
		"	 atan2 = math.atan2,"
		"	 ceil = math.ceil,"
		"	 cos = math.cos,"
		"	 cosh = math.cosh,"
		"	 deg = math.deg,"
		"	 exp = math.exp,"
		"	 floor = math.floor,"
		"	 fmod = math.fmod,"
		"	 frexp = math.frexp,"
		"	 huge = math.huge,"
		"	 ldexp = math.ldexp,"
		"	 log = math.log,"
		"	 log10 = math.log10,"
		"	 max = math.max,"
		"	 min = math.min,"
		"	 modf = math.modf,"
		"	 pi = math.pi,"
		"	 pow = math.pow,"
		"	 rad = math.rad,"
		"	 random = math.random,"
		"	 randomseed = math.randomseed,"
		"	 sin = math.sin,"
		"	 sinh = math.sinh,"
		"	 sqrt = math.sqrt,"
		"	 tan = math.tan,"
		"	 tanh = math.tanh },"
		"os = { clock = os.clock,"
		"	   date = os.date,"
		"	   difftime = os.difftime,"
		"	   time = os.time },"
		"string = { byte = string.byte,"
		"		   char = string.char,"
		"		   find = string.find,"
		"		   format = string.format,"
		"		   gmatch = string.gmatch,"
		"		   gsub = string.gsub,"
		"		   len = string.len,"
		"		   lower = string.lower,"
		"		   match = string.match,"
		"		   rep = string.rep,"
		"		   reverse = string.reverse,"
		"		   sub = string.sub,"
		"		   upper = string.upper },"
		"table = { insert = table.insert,"
		"		  maxn = table.maxn,"
		"		  remove = table.remove,"
		"		  sort = table.sort }"
	);

	std::string Lua_sandboxing_script(
		"sandbox_env = { " + Lua_function_whitelist + " } "
		"_ENV = sandbox_env "
	);

	return Lua_sandboxing_script;
}

void Lua_Level_Loader::_print_Lua_script_error(lua_State* L, std::string& levelstring)
{
	std::string Lua_error(lua_tostring(L, -1));
	Lua_error = Lua_error.substr(Lua_error.find(':')+1, Lua_error.length());
	std::cerr << "\nLua script error in " << levelstring << ":\n"
			  << "line " << Lua_error << "\n" << std::endl;
	lua_pop(L, 1);
	lua_close(L);

	return;
}

unsigned int Lua_Level_Loader::get_levelnumber()
{
	return _levelnumber;
}

bool fexists(std::string& filename)
{
	std::ifstream ifile(filename);
	return ifile;
}

bool Lua_Level_Loader::load_level( def::OpenGL3_Context& opengl3_context,
								   unsigned int levelnumber,
								   std::string levelpack )
{
	std::string Lua_sandboxing_script( // get a string of Lua code that starts
		_get_Lua_sandboxing_script()   // a sandbox environment
	);

	std::stringstream levelnumberss; // convert _levelnumber to a string
	levelnumberss << std::setw(3) << std::setfill('0') << _levelnumber;

	_levelstring = "levels/" + levelpack + "/" + levelnumberss.str() +
							"/level.lua"; // relative path to level.lua script

	_Lua_level_script.open(_levelstring); // save the contents of level.lua
										  // into _Lua_level_script

	#ifndef COMPILING_FOR_WINDOWS	// if compiling for Linux
		stat(_levelstring.c_str(), &_level_script_attributes); // store level file last modified time
	#else	// if compiling for Windows
		_stat(_levelstring.c_str(), &_level_script_attributes); // store level file last modified time
	#endif

	_level_script_old_attributes = _level_script_attributes;

	// attach the sandboxing code to the beginning of level.lua
	std::string Lua_sandboxed_level_script(Lua_sandboxing_script +
										   _Lua_level_script.content());

	// run the sandboxed script and check for syntax errors
	if (luaL_dostring(_sandboxed_L, Lua_sandboxed_level_script.c_str()))
	{
		_print_Lua_script_error(_sandboxed_L, _levelstring);
		return false;
	}

	lua_getglobal(_sandboxed_L, "sandbox_env"); // get the sandbox environment table
	if (!lua_istable(_sandboxed_L, -1))			// (which is the only global)
	{
		std::cerr << "\nLua script error in " << _levelstring << ":\n"
				  << "Error: global variable \"sandbox_env\" is not a table\n" << std::endl;
		lua_pop(_sandboxed_L, 1);
		lua_close(_sandboxed_L);
		return false;
	}

	lua_getfield(_sandboxed_L, -1, "level"); // get the level table
	if (!lua_istable(_sandboxed_L, -1))
	{
		std::cerr << "\nLua script error in " << _levelstring << ":\n"
				  << "Error: variable \"level\" is not a table\n" << std::endl;
		lua_pop(_sandboxed_L, 1);
		lua_close(_sandboxed_L);
		return false;
	}

	lua_getfield(_sandboxed_L, -1, "objects"); // get the objects table
	if (!lua_istable(_sandboxed_L, -1))
	{
		std::cerr << "\nLua script error in " << _levelstring << ":\n"
				  << "Error: variable \"objects\" is not a table\n" << std::endl;
		lua_pop(_sandboxed_L, 1);
		lua_close(_sandboxed_L);
		return false;
	}

	// get the length of the objects table so we can iterate over it
	_num_objects = luaL_len(_sandboxed_L, -1);

	objects.reset( new def::RR_Object[_num_objects] );


	for (unsigned int i = 1; i <= _num_objects; i++) // load the objects into
	{												 // the engine
		lua_rawgeti(_sandboxed_L, -1, i);
		lua_getfield(_sandboxed_L, -1, "id");
		if (!lua_isstring(_sandboxed_L, -1))
		{
			std::cerr << "\nLua script error in " << _levelstring << ":\n"
					  << "Error: object " << i << " has an incorrect or missing \"id\" string\n" << std::endl;
			lua_pop(_sandboxed_L, 1);
			lua_close(_sandboxed_L);
			return false;
		}

		const char* id(	// get id string and output "object loaded" message
			lua_tostring( _sandboxed_L, -1 )
		);
		std::cout << "object " << i << " loaded: " << id << std::endl;

		std::string model_filename("levels/" + levelpack + "/" + levelnumberss.str() +
									"/objects/" + id + ".dae");
		std::string texture_filename("levels/" + levelpack + "/" + levelnumberss.str() +
									 "/objects/" + id + ".png");

//		//----advanced shader stuff that isn't working yet-----------------
//		std::string obj_vert_shader_filename("levels/" + levelpack + "/" + levelnumberss.str() +
//											 "/shaders/" + id + ".vert");
//		std::string obj_frag_shader_filename("levels/" + levelpack + "/" + levelnumberss.str() +
//										 	 "/shaders/" + id + ".frag");
//
//		std::string level_vert_shader_filename("levels/" + levelpack + "/" + levelnumberss.str() +
//											   "/shaders/default.vert");
//		std::string level_frag_shader_filename("levels/" + levelpack + "/" + levelnumberss.str() +
//											   "/shaders/default.frag");
//
//		std::string default_vert_shader_filename("shaders/default.vert");
//		std::string default_frag_shader_filename("shaders/default.frag");
//
//		std::string vert_shader_filename;
//		std::string frag_shader_filename;
//
//		if (fexists(obj_vert_shader_filename))
//		{
//			vert_shader_filename = obj_vert_shader_filename;
//		}
//		else if (fexists(level_vert_shader_filename))
//		{
//			vert_shader_filename = level_vert_shader_filename;
//		}
//		else
//		{
//			vert_shader_filename = default_vert_shader_filename;
//		}
//
//		if (fexists(obj_frag_shader_filename))
//		{
//			frag_shader_filename = obj_frag_shader_filename;
//		}
//		else if (fexists(level_frag_shader_filename))
//		{
//			frag_shader_filename = level_frag_shader_filename;
//		}
//		else
//		{
//			frag_shader_filename = default_frag_shader_filename;
//		}
//
//		objects[i-1].set_vert_shader(vert_shader_filename);
//		objects[i-1].set_frag_shader(frag_shader_filename);
//
//		if (!objects[i-1].uses_default_vert_shader() || !objects[i-1].uses_default_frag_shader())
//			objects[i-1].init_shaders();
//		//--------end advanced shader stuff that isn't working yet--------

		lua_pop(_sandboxed_L, 1); // remove the id field from the Lua stack

		lua_getfield(_sandboxed_L, -1, "pos"); // try to get the object's position table
		if (!lua_istable(_sandboxed_L, -1) && !lua_isnil(_sandboxed_L, -1))
		{
			std::cerr << "\nLua script error in " << _levelstring << ":\n"
					  << "Error: object " << i << "'s \"pos\" element must be a table if present\n" << std::endl;
			lua_pop(_sandboxed_L, 1);
			lua_close(_sandboxed_L);
			return false;
		}

		if (!lua_isnil(_sandboxed_L, -1)) // if this object has a pos table defined in the level script
		{
			lua_len(_sandboxed_L, -1); // push pos table's length onto the Lua stack
			if (lua_tonumber(_sandboxed_L, -1) != 3)
			{
				std::cerr << "\nLua script error in " << _levelstring << ":\n"
						  << "Error: object " << i << "'s \"pos\" table must contain 3 numbers\n" << std::endl;
				lua_pop(_sandboxed_L, 1);
				lua_close(_sandboxed_L);
				return false;
			}
			lua_pop(_sandboxed_L, 1); // remove the position table's length from the Lua stack

			// make sure pos table's elements are numbers
			for (int j = 1; j <= 3; j++)
			{
				lua_rawgeti(_sandboxed_L, -1, j);
				if (!lua_isnumber(_sandboxed_L, -1))
				{
					std::cerr << "\nLua script error in " << _levelstring << ":\n"
							  << "Error: object " << i << "'s \"pos\" table's element " << j << " must be a number\n" << std::endl;
					lua_pop(_sandboxed_L, 1);
					lua_close(_sandboxed_L);
					return false;
				}
				if (j == 1)
					objects[i-1].set_x_pos(lua_tonumber(_sandboxed_L, -1));
				else if (j == 2)
					objects[i-1].set_y_pos(lua_tonumber(_sandboxed_L, -1));
				else if (j == 3)
					objects[i-1].set_z_pos(lua_tonumber(_sandboxed_L, -1));

				lua_pop(_sandboxed_L, 1); // remove the position table's element from the Lua stack
			}
		}
		else // if this object has no position table defined in the Lua level script,
		{	 // set its position to the origin ( 0, 0, 0 )
			objects[i-1].set_pos(0, 0, 0);
		}
		lua_pop(_sandboxed_L, 1); // remove the position table (or nil) from the Lua stack

		lua_getfield(_sandboxed_L, -1, "rot"); // try to get the object's rotation field
		if (!lua_isnumber(_sandboxed_L, -1) && !lua_isnil(_sandboxed_L, -1))
		{
			std::cerr << "\nLua script error in " << _levelstring << ":\n"
					  << "Error: object " << i << "'s \"rot\" element must be a number if present\n" << std::endl;
			lua_pop(_sandboxed_L, 1);
			lua_close(_sandboxed_L);
			return false;
		}
		objects[i-1].set_rot(lua_tonumber(_sandboxed_L, -1));

		objects[i-1].load(model_filename, texture_filename);

		lua_pop(_sandboxed_L, 2); // remove the rotation number and object table from the Lua stack

		std::cout << "object " << i << " position: ( "
				  << objects[i-1].get_x_pos() << ", "
				  << objects[i-1].get_y_pos() << ", "
				  << objects[i-1].get_z_pos() << " )"
				  << std::endl
				  << "object " << i << " rotation: " << objects[i-1].get_rot() << std::endl;
	}
	lua_pop(_sandboxed_L, 3);

	return true;
}

double Lua_Level_Loader::diffpos(glm::vec3& a, glm::vec3& b)
{
	return sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2) + pow(b.z - a.z, 2));
}

// In-place Quicksort
int partition(std::vector< std::pair< unsigned int, double > >& array, unsigned int left, unsigned int right, unsigned int pivotIndex)
{
	unsigned int pivotValue(array[pivotIndex].second);
	std::swap(array[pivotIndex], array[right]);
	unsigned int storeIndex(left);

	for (unsigned int i(left); i < right; i++)
	{
		if (array[i].second < pivotValue)
		{
			std::swap(array[i], array[storeIndex++]);
		}
	}
	std::swap(array[storeIndex], array[right]);

	return storeIndex;
}

void quicksort(std::vector< std::pair< unsigned int, double > >& array, unsigned int left, unsigned int right)
{
	if (left < right)
	{
		unsigned int pivotIndex(left+(right-left)/2);
		unsigned int pivotNewIndex(partition(array, left, right, pivotIndex));

		quicksort(array, left, pivotNewIndex);
		quicksort(array, pivotNewIndex+1, right);
	}

	return;
}

void Lua_Level_Loader::sort_objects( std::unique_ptr< def::RR_Object[] >& objects_to_sort, std::vector< std::pair< unsigned int, double > >& distances_from_camera )
{
	quicksort(distances_from_camera, 0, _num_objects-1);

//	// debug output
//	std::cout << "object distances from camera: ";
//	std::vector< std::pair< unsigned int, double > >::iterator it;
//	for (it = distances_from_camera.begin(); it != distances_from_camera.end(); it++)
//	{
//		std::cout << (*it).first << ": " << (*it).second << ", ";
//	}
//	std::cout << std::endl;

	return;
}

void Lua_Level_Loader::display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	if (!_paused)
	{
		_view_matrix = glm::lookAt( _position, _position+_direction, _up );
	}
	else // if paused
	{
		_view_matrix = glm::mat4(1.0f);
	}

	lua_getglobal(_sandboxed_L, "sandbox_env"); // get the sandbox environment table
	if (!lua_istable(_sandboxed_L, -1))			// (which is the only global)
	{
		std::cerr << "\nLua script error in " << _levelstring << ":\n"
				  << "Error: global variable \"sandbox_env\" is not a table\n" << std::endl;
		lua_pop(_sandboxed_L, 1);
		lua_close(_sandboxed_L);
		return;
	}

	lua_getfield(_sandboxed_L, -1, "level"); // get the level table
	if (!lua_istable(_sandboxed_L, -1))
	{
		std::cerr << "\nLua script error in " << _levelstring << ":\n"
				  << "Error: variable \"level\" is not a table\n" << std::endl;
		lua_pop(_sandboxed_L, 1);
		lua_close(_sandboxed_L);
		return;
	}

	lua_getfield(_sandboxed_L, -1, "main_loop"); // get the main_loop() function from level.lua
	if (!lua_isfunction(_sandboxed_L, -1))
	{
		std::cerr << "\nLua script error in " << _levelstring << ":\n"
				  << "Error: variable \"main_loop\" is not a function\n" << std::endl;
		lua_pop(_sandboxed_L, 1);
		lua_close(_sandboxed_L);
		return;
	}

	// call the main_loop() function
	lua_pcall(_sandboxed_L, 0, 0, 0);

	_modelview_matrix = _view_matrix * _model_matrix;
	_modelviewprojection_matrix = _projection_matrix * _modelview_matrix;

	_shaders.bind();

		glUniformMatrix4fv(_projection_matrix_location, 1, GL_FALSE, &_projection_matrix[0][0]);
		glUniformMatrix4fv(_view_matrix_location, 1, GL_FALSE, &_view_matrix[0][0]);
		glUniformMatrix4fv(_model_matrix_location, 1, GL_FALSE, &_model_matrix[0][0]);
		glUniformMatrix4fv(_modelview_matrix_location, 1, GL_FALSE, &_modelview_matrix[0][0]);
		glUniformMatrix4fv(_modelviewprojection_matrix_location, 1, GL_FALSE, &_modelviewprojection_matrix[0][0]);
		glUniformMatrix3fv(_normal_matrix_location, 1, GL_FALSE, &_normal_matrix[0][0]);
		glUniform4fv(_global_ambient_location, 1, &_global_ambient[0]);
		glUniform4fv(_light_ambient_location, 1, &_light_ambient[0]);
		glUniform4fv(_light_diffuse_location, 1, &_light_diffuse[0]);
		glUniform4fv(_light_specular_location, 1, &_light_specular[0]);
		glUniform4fv(_light_position_location, 1, &_light_position[0]);
		glUniform1i(_texture_sampler_location, 0);

		std::vector< std::pair< unsigned int, double > > object_distances;

		if (!_paused)	// if unpaused
		{
			lua_getfield(_sandboxed_L, -1, "objects"); // get the objects table
			if (!lua_istable(_sandboxed_L, -1))
			{
				std::cerr << "\nLua script error in " << _levelstring << ":\n"
						  << "Error: variable \"objects\" is not a table\n" << std::endl;
				lua_pop(_sandboxed_L, 1);
				lua_close(_sandboxed_L);
				return;
			}

			for (unsigned int i = 1; i <= _num_objects; i++)
			{
				// update object positions
				lua_rawgeti(_sandboxed_L, -1, i);
				lua_getfield(_sandboxed_L, -1, "id");
				if (!lua_isstring(_sandboxed_L, -1))
				{
					std::cerr << "\nLua script error in " << _levelstring << ":\n"
							  << "Error: object " << i << " has an incorrect or missing \"id\" string\n" << std::endl;
					lua_pop(_sandboxed_L, 1);
					lua_close(_sandboxed_L);
					return;
				}
				lua_pop(_sandboxed_L, 1); // remove object's id field from Lua stack

				lua_getfield(_sandboxed_L, -1, "pos"); // try to get the object's position table
				if (!lua_istable(_sandboxed_L, -1) && !lua_isnil(_sandboxed_L, -1))
				{
					std::cerr << "\nLua script error in " << _levelstring << ":\n"
							  << "Error: object " << i << "'s \"pos\" element must be a table if present\n" << std::endl;
					lua_pop(_sandboxed_L, 1);
					lua_close(_sandboxed_L);
					return;
				}

				if (!lua_isnil(_sandboxed_L, -1)) // if this object has a pos table defined in the level script
				{
					lua_len(_sandboxed_L, -1); // push pos table's length onto the Lua stack
					if (lua_tonumber(_sandboxed_L, -1) != 3)
					{
						std::cerr << "\nLua script error in " << _levelstring << ":\n"
								  << "Error: object " << i << "'s \"pos\" table must contain 3 numbers\n" << std::endl;
						lua_pop(_sandboxed_L, 1);
						lua_close(_sandboxed_L);
						return;
					}
					lua_pop(_sandboxed_L, 1); // remove the position table's length from the Lua stack

					// make sure pos table's elements are numbers
					for (int j = 1; j <= 3; j++)
					{
						lua_rawgeti(_sandboxed_L, -1, j);
						if (!lua_isnumber(_sandboxed_L, -1))
						{
							std::cerr << "\nLua script error in " << _levelstring << ":\n"
									  << "Error: object " << i << "'s \"pos\" table's element " << j << " must be a number\n" << std::endl;
							lua_pop(_sandboxed_L, 1);
							lua_close(_sandboxed_L);
							return;
						}
						if (j == 1)
							objects[i-1].set_x_pos(lua_tonumber(_sandboxed_L, -1));
						else if (j == 2)
							objects[i-1].set_y_pos(lua_tonumber(_sandboxed_L, -1));
						else if (j == 3)
							objects[i-1].set_z_pos(lua_tonumber(_sandboxed_L, -1));

						lua_pop(_sandboxed_L, 1); // remove the position table's element from the Lua stack
					}
				}
				else // if this object has no position table defined in the Lua level script,
				{	 // set its position to the origin ( 0, 0, 0 )
					objects[i-1].set_pos(0, 0, 0);
				}
				lua_pop(_sandboxed_L, 1); // remove position table from the Lua stack

				lua_getfield(_sandboxed_L, -1, "rot"); // try to get the object's rotation field
				if (!lua_isnumber(_sandboxed_L, -1) && !lua_isnil(_sandboxed_L, -1))
				{
					std::cerr << "\nLua script error in " << _levelstring << ":\n"
							  << "Error: object " << i << "'s \"rot\" element must be a number if present\n" << std::endl;
					lua_pop(_sandboxed_L, 1);
					lua_close(_sandboxed_L);
					return;
				}
				objects[i-1].set_rot(lua_tonumber(_sandboxed_L, -1));

				lua_pop(_sandboxed_L, 2); // remove the rotation and object tables from the Lua stack

				// make a list of object distances from the player
				glm::vec3 object_position( objects[i-1].get_x_pos(), objects[i-1].get_y_pos(), objects[i-1].get_z_pos() );
				object_distances.push_back( std::make_pair(i-1, diffpos(_position, object_position)) );
			}

			lua_pop(_sandboxed_L, 1); // remove objects table from Lua stack

			// sort objects from nearest to farthest
			sort_objects(objects, object_distances);
		}
		else	// if paused
		{
			_shaders.unbind();
				pause_menu.pm_display();
			_shaders.bind();
		}

		lua_pop(_sandboxed_L, 2); // remove sandbox_env and level from Lua stack

		// display everything in reverse order (farthest to nearest)
		std::vector< std::pair< unsigned int, double > >::reverse_iterator rit;
		for (rit = object_distances.rbegin(); rit != object_distances.rend(); rit++)
		{
			if (!_paused)
			{
				objects[(*rit).first].display(_shaders);

				if (_rocketpack_is_on)
				{
					_shaders.unbind();
						hud.hud_display_rp_on_icon();
					_shaders.bind();
				}
			}
			else // if paused
			{

			}
		}

	_shaders.unbind();

	// check if level script has been updated
	#ifndef COMPILING_FOR_WINDOWS	// if compiling for Linux
		stat(_levelstring.c_str(), &_level_script_attributes);
	#else	// if compiling for Windows
		_stat(_levelstring.c_str(), &_level_script_attributes);
	#endif

	// update level if level script has been modified since the game has started
	if (_level_script_attributes.st_mtime > _level_script_old_attributes.st_mtime)
	{
		std::cout << std::endl
				  << "Level script modified, reloading level!" << std::endl
				  << std::endl;


		sf::sleep(sf::seconds(4)); // allow time for modified level script to be written to disk

		load_level(_opengl3_context, _levelnumber, _levelpack);
	}

	return;
}

void Lua_Level_Loader::handle_events(float& time)
{
	// pause/menu
	if (!_paused && sf::Keyboard::isKeyPressed(_controls.Pause))
	{
		//_projection_matrix = glm::ortho(0.0f, (GLfloat)_resolution_width, (GLfloat)_resolution_height, 0.0f, 0.1f, 100.0f);
		_paused = true;
		sf::sleep(sf::seconds(0.22));
		sf::Mouse::setPosition(_middle_of_window, *_window);
		_window->setMouseCursorVisible(1);
		return;
	}
	else if (_paused && sf::Keyboard::isKeyPressed(_controls.Pause))
	{
		//_projection_matrix = glm::perspective(_fov, (GLfloat)_resolution_width / (GLfloat)_resolution_height, 0.1f, 500.0f);
		_paused = false;
		sf::sleep(sf::seconds(0.22));
		sf::Vector2i mouse_last_pos(_mouse_last_x_pos, _mouse_last_y_pos);
		sf::Mouse::setPosition(mouse_last_pos, *_window);
		_window->setMouseCursorVisible(0);
		return;
	}

	if (_paused)
	{
		pause_menu.pm_handle_events(time);	// handle pause menu events
	}
	else	// if unpaused
	{
		// camera stuff
		_mouse_last_x_pos = _mouse_x_pos;
		_mouse_last_y_pos = _mouse_y_pos;

		_mouse_x_pos = sf::Mouse::getPosition(*_window).x;
		_mouse_y_pos = sf::Mouse::getPosition(*_window).y;

	//	std::cout << "mouse x: " << _mouse_x_pos << std::endl;
	//	std::cout << "mouse y: " << _mouse_y_pos << std::endl;

		_horizontal_angle += time * _mouse_speed * float(_mouse_last_x_pos - _mouse_x_pos);
		_vertical_angle += time * _mouse_speed * float(_mouse_last_y_pos - _mouse_y_pos);

		// clamp rotation angle between 0 - 2*PI
		if (_horizontal_angle > 3.14f*2) _horizontal_angle = 0;
		if (_horizontal_angle < 0) _horizontal_angle = 3.14f*2;

		// clamp camera up/down values so we can't go upside down
		if (_vertical_angle >= 3.14f/2.0f) _vertical_angle = 3.14f/2.0f;
		if (_vertical_angle <= -3.14f/2.0f) _vertical_angle = -3.14f/2.0f;

	//	std::cout << "horiz angle: " << _horizontal_angle << std::endl;
	//	std::cout << "vert angle: " << _vertical_angle << std::endl;

		_direction = glm::vec3( cos(_vertical_angle) * sin(_horizontal_angle),
								sin(_vertical_angle),
								cos(_vertical_angle) * cos(_horizontal_angle) );

		_right = glm::vec3( sin(_horizontal_angle - 3.14f/2.0f),
							0,
							cos(_horizontal_angle - 3.14f/2.0f) );

		_up = glm::cross( _right, _direction );

		// keyboard: left, right, up, down
		if (sf::Keyboard::isKeyPressed(_controls.Left))
		{
			if (_rocketpack_is_on && sf::Keyboard::isKeyPressed(_controls.Jump))
			{
				_position -= _right * time * _rocketpack_speed * ((_rocketpack_up_speed/2) + 0.1f);
			}
			else if (_jumping)
			{
				_position -= _right * time * _speed * ((_jump_speed/2) + 0.1f);
			}
			else
			{
				_position -= _right * time * _speed;
			}
		}
		if (sf::Keyboard::isKeyPressed(_controls.Right))
		{
			if (_rocketpack_is_on && sf::Keyboard::isKeyPressed(_controls.Jump))
			{
				_position += _right * time * _rocketpack_speed * ((_rocketpack_up_speed/2) + 0.1f);
			}
			else if (_jumping)
			{
				_position += _right * time * _speed * ((_jump_speed/2) + 0.1f);
			}
			else
			{
				_position += _right * time * _speed;
			}
		}
		if (sf::Keyboard::isKeyPressed(_controls.Up))
		{
			glm::vec3 old_direction(_direction);
			_direction.y = 0.0f;

			if (_rocketpack_is_on && sf::Keyboard::isKeyPressed(_controls.Jump))
			{
				_position += _direction * time * _rocketpack_speed * ((_rocketpack_up_speed/2) + 0.1f);
			}
			else if (_jumping)
			{
				_position += _direction * time * _speed * ((_jump_speed/2) + 0.1f);
			}
			else
			{
				_position += _direction * time * _speed;
			}

			_direction = old_direction;
		}
		if (sf::Keyboard::isKeyPressed(_controls.Down))
		{
			glm::vec3 old_direction(_direction);
			_direction.y = 0.0f;

			if (_rocketpack_is_on && sf::Keyboard::isKeyPressed(_controls.Jump))
				_position -= _direction * time * _rocketpack_speed * ((_rocketpack_up_speed/2) + 0.1f);
			if (_jumping)
				_position -= _direction * time * _speed * ((_jump_speed/2) + 0.1f);
			else
				_position -= _direction * time * _speed;

			_direction = old_direction;
		}

		// keyboard: jump
		if (sf::Keyboard::isKeyPressed(_controls.Jump) && (_rocketpack_is_on || _stopped_jumping))
		{
			// check if standing on something
			if (_standing || _rocketpack_is_on)
			{
				_standing = false;
				_jumping = true;
				_stopped_jumping = false;
			}
		}

		if (!sf::Keyboard::isKeyPressed(_controls.Jump))
		{
			if (_standing)
			{
				_stopped_jumping = true;
			}
		}

		// apply gravity if off the ground
		if (_position.y > _main_character_height && !_jumping)
			_position.y -= time * _speed * _global_gravity;

		// if started jumping
		else if ((_position.y < _main_character_height + _jump_height - 3.0f || _rocketpack_is_on) && _jumping)
		{
			if (sf::Keyboard::isKeyPressed(_controls.Jump))
			{
				if (_rocketpack_is_on)
					_position.y += time * _rocketpack_speed * _rocketpack_up_speed;
				else
					_position.y += time * _speed * _jump_speed;
			}
			else	// if stopped jumping
			{
				_position += _direction * time * (_speed/(_jump_hang_time*2));
				_jumping = false;
			}
		}

		// if near the highest part of the jump
		else if (_position.y <= _main_character_height + _jump_height && _jumping)
		{
			if (sf::Keyboard::isKeyPressed(_controls.Jump))
			{
				if (_rocketpack_is_on)
					_position.y += time * _rocketpack_speed * (_rocketpack_up_speed/_jump_hang_time);
				else
					_position.y += time * _speed * (_jump_speed/_jump_hang_time);
			}
			else	// if stopped jumping
			{
				_position += _direction * time * (_speed/(_jump_hang_time*2));
				_jumping = false;
			}
		}

		// if reached the highest part of the jump
		else if (_position.y >= _main_character_height + _jump_height)
		{
			// if rocketpack is on, don't start falling
			if (_rocketpack_is_on && sf::Keyboard::isKeyPressed(_controls.Jump))
			{
				_position += _direction * time * (_rocketpack_speed/_jump_hang_time);
			}
			else
			{
				_position += _direction * time * (_speed/_jump_hang_time);
				_jumping = false;
			}
		}
		else if (_position.y <= _main_character_height)
		{
			_standing = true;
		}

		// temporary hack to not fall through the floor
		if (_position.y < _main_character_height)
			_position.y = _main_character_height;

		//_fov = _initial_fov - 5;	// set amount of zoom

		//_projection_matrix = glm::perspective(_fov, 16.0f/9.0f, 0.1f, 100.0f); // this allows for adjustable zoom

		if ( _mouse_x_pos <= 0 || _mouse_x_pos >= _resolution_width-1 ||
			 _mouse_y_pos <= 0 || _mouse_y_pos >= _resolution_height-1 )
		{
			sf::Mouse::setPosition(_middle_of_window, *_window);
			_mouse_x_pos = _mouse_last_x_pos = _resolution_width/2;
			_mouse_y_pos = _mouse_last_y_pos = _resolution_height/2;
		}
	}

	// toggle the rocketpack off
	if (sf::Keyboard::isKeyPressed(_controls.Rocketpack) && _rocketpack_is_on && _rocketpack_toggle_counter == 0)
	{
		_rocketpack_is_on = false;
		_rocketpack_toggle_counter = 30;
	}
	// toggle the rocketpack on
	else if (sf::Keyboard::isKeyPressed(_controls.Rocketpack) && !_rocketpack_is_on && _rocketpack_toggle_counter == 0)
	{
		_rocketpack_is_on = true;
		_rocketpack_toggle_counter = 30;
	}
	if (_rocketpack_toggle_counter > 0)
		_rocketpack_toggle_counter--;
}

}
