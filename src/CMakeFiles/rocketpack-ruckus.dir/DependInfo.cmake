# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/main.cpp" "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/rocketpack-ruckus.dir/main.cpp.o"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/main_loop.cpp" "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/rocketpack-ruckus.dir/main_loop.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defFile.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defShader.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defAssimp_Resource.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defVideo.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defOpenGL3_Context.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defControls.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defRR_Player.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defRR_Object.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defRR_Pause_Menu.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defRR_HUD.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defLua_Level_Loader.dir/DependInfo.cmake"
  "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus/src/CMakeFiles/defmain_events.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "src/lua/src"
  "."
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
