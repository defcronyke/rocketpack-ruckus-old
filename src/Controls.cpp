/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Controls.cpp
 *
 *  Created on: 2012-07-13
 *      Author: Jeremy Carter
 */

#include "../include/Controls.hpp"

namespace def
{

/** Parses controls.conf and maps the controls
 	to their corresponding SFML buttons. */
Controls::Controls(const std::string& controls_conf_file)	///< [in] the controls.conf file
	: _controls_conf(controls_conf_file)
{
	std::stringstream ss;

	for (unsigned int i = 0; i < _controls_conf.content().length(); i++)
	{
		if (_controls_conf.content()[i] != '#')
			ss << _controls_conf.content()[i];
		else
		{
			i = _controls_conf.content().find('\n', i);
			ss << '\n';
		}
	}

	std::string line;

	while (getline(ss, line))
	{
		if (line != "")
		{
			int colon_pos = line.find(':', 0);
			std::string before_colon(line.substr(0, colon_pos));
			std::string after_colon(line.substr(colon_pos+1, line.length()-colon_pos));
			boost::trim(before_colon);
			boost::trim(after_colon);
			controls_conf_map[before_colon] = after_colon;
		}
	}

	_map_controls();
}

/** get the code for this function by running
 	scripts/generate_map_controls.py */
void Controls::_map_controls()
{
	if (controls_conf_map.find("Left") != controls_conf_map.end())
	{
		if (controls_conf_map["Left"] == "A")
			Left = sf::Keyboard::A;
		if (controls_conf_map["Left"] == "B")
			Left = sf::Keyboard::B;
		if (controls_conf_map["Left"] == "C")
			Left = sf::Keyboard::C;
		if (controls_conf_map["Left"] == "D")
			Left = sf::Keyboard::D;
		if (controls_conf_map["Left"] == "E")
			Left = sf::Keyboard::E;
		if (controls_conf_map["Left"] == "F")
			Left = sf::Keyboard::F;
		if (controls_conf_map["Left"] == "G")
			Left = sf::Keyboard::G;
		if (controls_conf_map["Left"] == "H")
			Left = sf::Keyboard::H;
		if (controls_conf_map["Left"] == "I")
			Left = sf::Keyboard::I;
		if (controls_conf_map["Left"] == "J")
			Left = sf::Keyboard::J;
		if (controls_conf_map["Left"] == "K")
			Left = sf::Keyboard::K;
		if (controls_conf_map["Left"] == "L")
			Left = sf::Keyboard::L;
		if (controls_conf_map["Left"] == "M")
			Left = sf::Keyboard::M;
		if (controls_conf_map["Left"] == "N")
			Left = sf::Keyboard::N;
		if (controls_conf_map["Left"] == "O")
			Left = sf::Keyboard::O;
		if (controls_conf_map["Left"] == "P")
			Left = sf::Keyboard::P;
		if (controls_conf_map["Left"] == "Q")
			Left = sf::Keyboard::Q;
		if (controls_conf_map["Left"] == "R")
			Left = sf::Keyboard::R;
		if (controls_conf_map["Left"] == "S")
			Left = sf::Keyboard::S;
		if (controls_conf_map["Left"] == "T")
			Left = sf::Keyboard::T;
		if (controls_conf_map["Left"] == "U")
			Left = sf::Keyboard::U;
		if (controls_conf_map["Left"] == "V")
			Left = sf::Keyboard::V;
		if (controls_conf_map["Left"] == "W")
			Left = sf::Keyboard::W;
		if (controls_conf_map["Left"] == "X")
			Left = sf::Keyboard::X;
		if (controls_conf_map["Left"] == "Y")
			Left = sf::Keyboard::Y;
		if (controls_conf_map["Left"] == "Z")
			Left = sf::Keyboard::Z;
		if (controls_conf_map["Left"] == "Num0")
			Left = sf::Keyboard::Num0;
		if (controls_conf_map["Left"] == "Numpad0")
			Left = sf::Keyboard::Numpad0;
		if (controls_conf_map["Left"] == "Num1")
			Left = sf::Keyboard::Num1;
		if (controls_conf_map["Left"] == "Numpad1")
			Left = sf::Keyboard::Numpad1;
		if (controls_conf_map["Left"] == "Num2")
			Left = sf::Keyboard::Num2;
		if (controls_conf_map["Left"] == "Numpad2")
			Left = sf::Keyboard::Numpad2;
		if (controls_conf_map["Left"] == "Num3")
			Left = sf::Keyboard::Num3;
		if (controls_conf_map["Left"] == "Numpad3")
			Left = sf::Keyboard::Numpad3;
		if (controls_conf_map["Left"] == "Num4")
			Left = sf::Keyboard::Num4;
		if (controls_conf_map["Left"] == "Numpad4")
			Left = sf::Keyboard::Numpad4;
		if (controls_conf_map["Left"] == "Num5")
			Left = sf::Keyboard::Num5;
		if (controls_conf_map["Left"] == "Numpad5")
			Left = sf::Keyboard::Numpad5;
		if (controls_conf_map["Left"] == "Num6")
			Left = sf::Keyboard::Num6;
		if (controls_conf_map["Left"] == "Numpad6")
			Left = sf::Keyboard::Numpad6;
		if (controls_conf_map["Left"] == "Num7")
			Left = sf::Keyboard::Num7;
		if (controls_conf_map["Left"] == "Numpad7")
			Left = sf::Keyboard::Numpad7;
		if (controls_conf_map["Left"] == "Num8")
			Left = sf::Keyboard::Num8;
		if (controls_conf_map["Left"] == "Numpad8")
			Left = sf::Keyboard::Numpad8;
		if (controls_conf_map["Left"] == "Num9")
			Left = sf::Keyboard::Num9;
		if (controls_conf_map["Left"] == "Numpad9")
			Left = sf::Keyboard::Numpad9;
		if (controls_conf_map["Left"] == "F1")
			Left = sf::Keyboard::F1;
		if (controls_conf_map["Left"] == "F2")
			Left = sf::Keyboard::F2;
		if (controls_conf_map["Left"] == "F3")
			Left = sf::Keyboard::F3;
		if (controls_conf_map["Left"] == "F4")
			Left = sf::Keyboard::F4;
		if (controls_conf_map["Left"] == "F5")
			Left = sf::Keyboard::F5;
		if (controls_conf_map["Left"] == "F6")
			Left = sf::Keyboard::F6;
		if (controls_conf_map["Left"] == "F7")
			Left = sf::Keyboard::F7;
		if (controls_conf_map["Left"] == "F8")
			Left = sf::Keyboard::F8;
		if (controls_conf_map["Left"] == "F9")
			Left = sf::Keyboard::F9;
		if (controls_conf_map["Left"] == "F10")
			Left = sf::Keyboard::F10;
		if (controls_conf_map["Left"] == "F11")
			Left = sf::Keyboard::F11;
		if (controls_conf_map["Left"] == "F12")
			Left = sf::Keyboard::F12;
		if (controls_conf_map["Left"] == "Space")
			Left = sf::Keyboard::Space;
		if (controls_conf_map["Left"] == "Left")
			Left = sf::Keyboard::Left;
		if (controls_conf_map["Left"] == "Right")
			Left = sf::Keyboard::Right;
		if (controls_conf_map["Left"] == "Up")
			Left = sf::Keyboard::Up;
		if (controls_conf_map["Left"] == "Down")
			Left = sf::Keyboard::Down;
		if (controls_conf_map["Left"] == "Dash")
			Left = sf::Keyboard::Dash;
		if (controls_conf_map["Left"] == "Equal")
			Left = sf::Keyboard::Equal;
		if (controls_conf_map["Left"] == "BackSpace")
			#ifdef COMPILING_FOR_WINDOWS
			Left = sf::Keyboard::Back;
			#else
			Left = sf::Keyboard::BackSpace;
			#endif
		if (controls_conf_map["Left"] == "Tab")
			Left = sf::Keyboard::Tab;
		if (controls_conf_map["Left"] == "SemiColon")
			Left = sf::Keyboard::SemiColon;
		if (controls_conf_map["Left"] == "Quote")
			Left = sf::Keyboard::Quote;
		if (controls_conf_map["Left"] == "Comma")
			Left = sf::Keyboard::Comma;
		if (controls_conf_map["Left"] == "Period")
			Left = sf::Keyboard::Period;
		if (controls_conf_map["Left"] == "Slash")
			Left = sf::Keyboard::Slash;
		if (controls_conf_map["Left"] == "LBracket")
			Left = sf::Keyboard::LBracket;
		if (controls_conf_map["Left"] == "RBracket")
			Left = sf::Keyboard::RBracket;
		if (controls_conf_map["Left"] == "BackSlash")
			Left = sf::Keyboard::BackSlash;
		if (controls_conf_map["Left"] == "Return")
			Left = sf::Keyboard::Return;
		if (controls_conf_map["Left"] == "LShift")
			Left = sf::Keyboard::LShift;
		if (controls_conf_map["Left"] == "RShift")
			Left = sf::Keyboard::RShift;
		if (controls_conf_map["Left"] == "LControl")
			Left = sf::Keyboard::LControl;
		if (controls_conf_map["Left"] == "RControl")
			Left = sf::Keyboard::RControl;
		if (controls_conf_map["Left"] == "LAlt")
			Left = sf::Keyboard::LAlt;
		if (controls_conf_map["Left"] == "RAlt")
			Left = sf::Keyboard::RAlt;
		if (controls_conf_map["Left"] == "Home")
			Left = sf::Keyboard::Home;
		if (controls_conf_map["Left"] == "End")
			Left = sf::Keyboard::End;
		if (controls_conf_map["Left"] == "Delete")
			Left = sf::Keyboard::Delete;
		if (controls_conf_map["Left"] == "PageUp")
			Left = sf::Keyboard::PageUp;
		if (controls_conf_map["Left"] == "PageDown")
			Left = sf::Keyboard::PageDown;
		if (controls_conf_map["Left"] == "Divide")
			Left = sf::Keyboard::Divide;
		if (controls_conf_map["Left"] == "Multiply")
			Left = sf::Keyboard::Multiply;
		if (controls_conf_map["Left"] == "Subtract")
			Left = sf::Keyboard::Subtract;
		if (controls_conf_map["Left"] == "Add")
			Left = sf::Keyboard::Add;
		if (controls_conf_map["Left"] == "LSystem")
			Left = sf::Keyboard::LSystem;
		if (controls_conf_map["Left"] == "RSystem")
			Left = sf::Keyboard::RSystem;
		if (controls_conf_map["Left"] == "Menu")
			Left = sf::Keyboard::Menu;
		if (controls_conf_map["Left"] == "Insert")
			Left = sf::Keyboard::Insert;
		if (controls_conf_map["Left"] == "Pause")
			Left = sf::Keyboard::Pause;
	}

	if (controls_conf_map.find("Right") != controls_conf_map.end())
	{
		if (controls_conf_map["Right"] == "A")
			Right = sf::Keyboard::A;
		if (controls_conf_map["Right"] == "B")
			Right = sf::Keyboard::B;
		if (controls_conf_map["Right"] == "C")
			Right = sf::Keyboard::C;
		if (controls_conf_map["Right"] == "D")
			Right = sf::Keyboard::D;
		if (controls_conf_map["Right"] == "E")
			Right = sf::Keyboard::E;
		if (controls_conf_map["Right"] == "F")
			Right = sf::Keyboard::F;
		if (controls_conf_map["Right"] == "G")
			Right = sf::Keyboard::G;
		if (controls_conf_map["Right"] == "H")
			Right = sf::Keyboard::H;
		if (controls_conf_map["Right"] == "I")
			Right = sf::Keyboard::I;
		if (controls_conf_map["Right"] == "J")
			Right = sf::Keyboard::J;
		if (controls_conf_map["Right"] == "K")
			Right = sf::Keyboard::K;
		if (controls_conf_map["Right"] == "L")
			Right = sf::Keyboard::L;
		if (controls_conf_map["Right"] == "M")
			Right = sf::Keyboard::M;
		if (controls_conf_map["Right"] == "N")
			Right = sf::Keyboard::N;
		if (controls_conf_map["Right"] == "O")
			Right = sf::Keyboard::O;
		if (controls_conf_map["Right"] == "P")
			Right = sf::Keyboard::P;
		if (controls_conf_map["Right"] == "Q")
			Right = sf::Keyboard::Q;
		if (controls_conf_map["Right"] == "R")
			Right = sf::Keyboard::R;
		if (controls_conf_map["Right"] == "S")
			Right = sf::Keyboard::S;
		if (controls_conf_map["Right"] == "T")
			Right = sf::Keyboard::T;
		if (controls_conf_map["Right"] == "U")
			Right = sf::Keyboard::U;
		if (controls_conf_map["Right"] == "V")
			Right = sf::Keyboard::V;
		if (controls_conf_map["Right"] == "W")
			Right = sf::Keyboard::W;
		if (controls_conf_map["Right"] == "X")
			Right = sf::Keyboard::X;
		if (controls_conf_map["Right"] == "Y")
			Right = sf::Keyboard::Y;
		if (controls_conf_map["Right"] == "Z")
			Right = sf::Keyboard::Z;
		if (controls_conf_map["Right"] == "Num0")
			Right = sf::Keyboard::Num0;
		if (controls_conf_map["Right"] == "Numpad0")
			Right = sf::Keyboard::Numpad0;
		if (controls_conf_map["Right"] == "Num1")
			Right = sf::Keyboard::Num1;
		if (controls_conf_map["Right"] == "Numpad1")
			Right = sf::Keyboard::Numpad1;
		if (controls_conf_map["Right"] == "Num2")
			Right = sf::Keyboard::Num2;
		if (controls_conf_map["Right"] == "Numpad2")
			Right = sf::Keyboard::Numpad2;
		if (controls_conf_map["Right"] == "Num3")
			Right = sf::Keyboard::Num3;
		if (controls_conf_map["Right"] == "Numpad3")
			Right = sf::Keyboard::Numpad3;
		if (controls_conf_map["Right"] == "Num4")
			Right = sf::Keyboard::Num4;
		if (controls_conf_map["Right"] == "Numpad4")
			Right = sf::Keyboard::Numpad4;
		if (controls_conf_map["Right"] == "Num5")
			Right = sf::Keyboard::Num5;
		if (controls_conf_map["Right"] == "Numpad5")
			Right = sf::Keyboard::Numpad5;
		if (controls_conf_map["Right"] == "Num6")
			Right = sf::Keyboard::Num6;
		if (controls_conf_map["Right"] == "Numpad6")
			Right = sf::Keyboard::Numpad6;
		if (controls_conf_map["Right"] == "Num7")
			Right = sf::Keyboard::Num7;
		if (controls_conf_map["Right"] == "Numpad7")
			Right = sf::Keyboard::Numpad7;
		if (controls_conf_map["Right"] == "Num8")
			Right = sf::Keyboard::Num8;
		if (controls_conf_map["Right"] == "Numpad8")
			Right = sf::Keyboard::Numpad8;
		if (controls_conf_map["Right"] == "Num9")
			Right = sf::Keyboard::Num9;
		if (controls_conf_map["Right"] == "Numpad9")
			Right = sf::Keyboard::Numpad9;
		if (controls_conf_map["Right"] == "F1")
			Right = sf::Keyboard::F1;
		if (controls_conf_map["Right"] == "F2")
			Right = sf::Keyboard::F2;
		if (controls_conf_map["Right"] == "F3")
			Right = sf::Keyboard::F3;
		if (controls_conf_map["Right"] == "F4")
			Right = sf::Keyboard::F4;
		if (controls_conf_map["Right"] == "F5")
			Right = sf::Keyboard::F5;
		if (controls_conf_map["Right"] == "F6")
			Right = sf::Keyboard::F6;
		if (controls_conf_map["Right"] == "F7")
			Right = sf::Keyboard::F7;
		if (controls_conf_map["Right"] == "F8")
			Right = sf::Keyboard::F8;
		if (controls_conf_map["Right"] == "F9")
			Right = sf::Keyboard::F9;
		if (controls_conf_map["Right"] == "F10")
			Right = sf::Keyboard::F10;
		if (controls_conf_map["Right"] == "F11")
			Right = sf::Keyboard::F11;
		if (controls_conf_map["Right"] == "F12")
			Right = sf::Keyboard::F12;
		if (controls_conf_map["Right"] == "Space")
			Right = sf::Keyboard::Space;
		if (controls_conf_map["Right"] == "Left")
			Right = sf::Keyboard::Left;
		if (controls_conf_map["Right"] == "Right")
			Right = sf::Keyboard::Right;
		if (controls_conf_map["Right"] == "Up")
			Right = sf::Keyboard::Up;
		if (controls_conf_map["Right"] == "Down")
			Right = sf::Keyboard::Down;
		if (controls_conf_map["Right"] == "Dash")
			Right = sf::Keyboard::Dash;
		if (controls_conf_map["Right"] == "Equal")
			Right = sf::Keyboard::Equal;
		if (controls_conf_map["Right"] == "BackSpace")
			#ifdef COMPILING_FOR_WINDOWS
			Right = sf::Keyboard::Back;
			#else
			Right = sf::Keyboard::BackSpace;
			#endif
		if (controls_conf_map["Right"] == "Tab")
			Right = sf::Keyboard::Tab;
		if (controls_conf_map["Right"] == "SemiColon")
			Right = sf::Keyboard::SemiColon;
		if (controls_conf_map["Right"] == "Quote")
			Right = sf::Keyboard::Quote;
		if (controls_conf_map["Right"] == "Comma")
			Right = sf::Keyboard::Comma;
		if (controls_conf_map["Right"] == "Period")
			Right = sf::Keyboard::Period;
		if (controls_conf_map["Right"] == "Slash")
			Right = sf::Keyboard::Slash;
		if (controls_conf_map["Right"] == "LBracket")
			Right = sf::Keyboard::LBracket;
		if (controls_conf_map["Right"] == "RBracket")
			Right = sf::Keyboard::RBracket;
		if (controls_conf_map["Right"] == "BackSlash")
			Right = sf::Keyboard::BackSlash;
		if (controls_conf_map["Right"] == "Return")
			Right = sf::Keyboard::Return;
		if (controls_conf_map["Right"] == "LShift")
			Right = sf::Keyboard::LShift;
		if (controls_conf_map["Right"] == "RShift")
			Right = sf::Keyboard::RShift;
		if (controls_conf_map["Right"] == "LControl")
			Right = sf::Keyboard::LControl;
		if (controls_conf_map["Right"] == "RControl")
			Right = sf::Keyboard::RControl;
		if (controls_conf_map["Right"] == "LAlt")
			Right = sf::Keyboard::LAlt;
		if (controls_conf_map["Right"] == "RAlt")
			Right = sf::Keyboard::RAlt;
		if (controls_conf_map["Right"] == "Home")
			Right = sf::Keyboard::Home;
		if (controls_conf_map["Right"] == "End")
			Right = sf::Keyboard::End;
		if (controls_conf_map["Right"] == "Delete")
			Right = sf::Keyboard::Delete;
		if (controls_conf_map["Right"] == "PageUp")
			Right = sf::Keyboard::PageUp;
		if (controls_conf_map["Right"] == "PageDown")
			Right = sf::Keyboard::PageDown;
		if (controls_conf_map["Right"] == "Divide")
			Right = sf::Keyboard::Divide;
		if (controls_conf_map["Right"] == "Multiply")
			Right = sf::Keyboard::Multiply;
		if (controls_conf_map["Right"] == "Subtract")
			Right = sf::Keyboard::Subtract;
		if (controls_conf_map["Right"] == "Add")
			Right = sf::Keyboard::Add;
		if (controls_conf_map["Right"] == "LSystem")
			Right = sf::Keyboard::LSystem;
		if (controls_conf_map["Right"] == "RSystem")
			Right = sf::Keyboard::RSystem;
		if (controls_conf_map["Right"] == "Menu")
			Right = sf::Keyboard::Menu;
		if (controls_conf_map["Right"] == "Insert")
			Right = sf::Keyboard::Insert;
		if (controls_conf_map["Right"] == "Pause")
			Right = sf::Keyboard::Pause;
	}

	if (controls_conf_map.find("Up") != controls_conf_map.end())
	{
		if (controls_conf_map["Up"] == "A")
			Up = sf::Keyboard::A;
		if (controls_conf_map["Up"] == "B")
			Up = sf::Keyboard::B;
		if (controls_conf_map["Up"] == "C")
			Up = sf::Keyboard::C;
		if (controls_conf_map["Up"] == "D")
			Up = sf::Keyboard::D;
		if (controls_conf_map["Up"] == "E")
			Up = sf::Keyboard::E;
		if (controls_conf_map["Up"] == "F")
			Up = sf::Keyboard::F;
		if (controls_conf_map["Up"] == "G")
			Up = sf::Keyboard::G;
		if (controls_conf_map["Up"] == "H")
			Up = sf::Keyboard::H;
		if (controls_conf_map["Up"] == "I")
			Up = sf::Keyboard::I;
		if (controls_conf_map["Up"] == "J")
			Up = sf::Keyboard::J;
		if (controls_conf_map["Up"] == "K")
			Up = sf::Keyboard::K;
		if (controls_conf_map["Up"] == "L")
			Up = sf::Keyboard::L;
		if (controls_conf_map["Up"] == "M")
			Up = sf::Keyboard::M;
		if (controls_conf_map["Up"] == "N")
			Up = sf::Keyboard::N;
		if (controls_conf_map["Up"] == "O")
			Up = sf::Keyboard::O;
		if (controls_conf_map["Up"] == "P")
			Up = sf::Keyboard::P;
		if (controls_conf_map["Up"] == "Q")
			Up = sf::Keyboard::Q;
		if (controls_conf_map["Up"] == "R")
			Up = sf::Keyboard::R;
		if (controls_conf_map["Up"] == "S")
			Up = sf::Keyboard::S;
		if (controls_conf_map["Up"] == "T")
			Up = sf::Keyboard::T;
		if (controls_conf_map["Up"] == "U")
			Up = sf::Keyboard::U;
		if (controls_conf_map["Up"] == "V")
			Up = sf::Keyboard::V;
		if (controls_conf_map["Up"] == "W")
			Up = sf::Keyboard::W;
		if (controls_conf_map["Up"] == "X")
			Up = sf::Keyboard::X;
		if (controls_conf_map["Up"] == "Y")
			Up = sf::Keyboard::Y;
		if (controls_conf_map["Up"] == "Z")
			Up = sf::Keyboard::Z;
		if (controls_conf_map["Up"] == "Num0")
			Up = sf::Keyboard::Num0;
		if (controls_conf_map["Up"] == "Numpad0")
			Up = sf::Keyboard::Numpad0;
		if (controls_conf_map["Up"] == "Num1")
			Up = sf::Keyboard::Num1;
		if (controls_conf_map["Up"] == "Numpad1")
			Up = sf::Keyboard::Numpad1;
		if (controls_conf_map["Up"] == "Num2")
			Up = sf::Keyboard::Num2;
		if (controls_conf_map["Up"] == "Numpad2")
			Up = sf::Keyboard::Numpad2;
		if (controls_conf_map["Up"] == "Num3")
			Up = sf::Keyboard::Num3;
		if (controls_conf_map["Up"] == "Numpad3")
			Up = sf::Keyboard::Numpad3;
		if (controls_conf_map["Up"] == "Num4")
			Up = sf::Keyboard::Num4;
		if (controls_conf_map["Up"] == "Numpad4")
			Up = sf::Keyboard::Numpad4;
		if (controls_conf_map["Up"] == "Num5")
			Up = sf::Keyboard::Num5;
		if (controls_conf_map["Up"] == "Numpad5")
			Up = sf::Keyboard::Numpad5;
		if (controls_conf_map["Up"] == "Num6")
			Up = sf::Keyboard::Num6;
		if (controls_conf_map["Up"] == "Numpad6")
			Up = sf::Keyboard::Numpad6;
		if (controls_conf_map["Up"] == "Num7")
			Up = sf::Keyboard::Num7;
		if (controls_conf_map["Up"] == "Numpad7")
			Up = sf::Keyboard::Numpad7;
		if (controls_conf_map["Up"] == "Num8")
			Up = sf::Keyboard::Num8;
		if (controls_conf_map["Up"] == "Numpad8")
			Up = sf::Keyboard::Numpad8;
		if (controls_conf_map["Up"] == "Num9")
			Up = sf::Keyboard::Num9;
		if (controls_conf_map["Up"] == "Numpad9")
			Up = sf::Keyboard::Numpad9;
		if (controls_conf_map["Up"] == "F1")
			Up = sf::Keyboard::F1;
		if (controls_conf_map["Up"] == "F2")
			Up = sf::Keyboard::F2;
		if (controls_conf_map["Up"] == "F3")
			Up = sf::Keyboard::F3;
		if (controls_conf_map["Up"] == "F4")
			Up = sf::Keyboard::F4;
		if (controls_conf_map["Up"] == "F5")
			Up = sf::Keyboard::F5;
		if (controls_conf_map["Up"] == "F6")
			Up = sf::Keyboard::F6;
		if (controls_conf_map["Up"] == "F7")
			Up = sf::Keyboard::F7;
		if (controls_conf_map["Up"] == "F8")
			Up = sf::Keyboard::F8;
		if (controls_conf_map["Up"] == "F9")
			Up = sf::Keyboard::F9;
		if (controls_conf_map["Up"] == "F10")
			Up = sf::Keyboard::F10;
		if (controls_conf_map["Up"] == "F11")
			Up = sf::Keyboard::F11;
		if (controls_conf_map["Up"] == "F12")
			Up = sf::Keyboard::F12;
		if (controls_conf_map["Up"] == "Space")
			Up = sf::Keyboard::Space;
		if (controls_conf_map["Up"] == "Left")
			Up = sf::Keyboard::Left;
		if (controls_conf_map["Up"] == "Right")
			Up = sf::Keyboard::Right;
		if (controls_conf_map["Up"] == "Up")
			Up = sf::Keyboard::Up;
		if (controls_conf_map["Up"] == "Down")
			Up = sf::Keyboard::Down;
		if (controls_conf_map["Up"] == "Dash")
			Up = sf::Keyboard::Dash;
		if (controls_conf_map["Up"] == "Equal")
			Up = sf::Keyboard::Equal;
		if (controls_conf_map["Up"] == "BackSpace")
			#ifdef COMPILING_FOR_WINDOWS
			Up = sf::Keyboard::Back;
			#else
			Up = sf::Keyboard::BackSpace;
			#endif
		if (controls_conf_map["Up"] == "Tab")
			Up = sf::Keyboard::Tab;
		if (controls_conf_map["Up"] == "SemiColon")
			Up = sf::Keyboard::SemiColon;
		if (controls_conf_map["Up"] == "Quote")
			Up = sf::Keyboard::Quote;
		if (controls_conf_map["Up"] == "Comma")
			Up = sf::Keyboard::Comma;
		if (controls_conf_map["Up"] == "Period")
			Up = sf::Keyboard::Period;
		if (controls_conf_map["Up"] == "Slash")
			Up = sf::Keyboard::Slash;
		if (controls_conf_map["Up"] == "LBracket")
			Up = sf::Keyboard::LBracket;
		if (controls_conf_map["Up"] == "RBracket")
			Up = sf::Keyboard::RBracket;
		if (controls_conf_map["Up"] == "BackSlash")
			Up = sf::Keyboard::BackSlash;
		if (controls_conf_map["Up"] == "Return")
			Up = sf::Keyboard::Return;
		if (controls_conf_map["Up"] == "LShift")
			Up = sf::Keyboard::LShift;
		if (controls_conf_map["Up"] == "RShift")
			Up = sf::Keyboard::RShift;
		if (controls_conf_map["Up"] == "LControl")
			Up = sf::Keyboard::LControl;
		if (controls_conf_map["Up"] == "RControl")
			Up = sf::Keyboard::RControl;
		if (controls_conf_map["Up"] == "LAlt")
			Up = sf::Keyboard::LAlt;
		if (controls_conf_map["Up"] == "RAlt")
			Up = sf::Keyboard::RAlt;
		if (controls_conf_map["Up"] == "Home")
			Up = sf::Keyboard::Home;
		if (controls_conf_map["Up"] == "End")
			Up = sf::Keyboard::End;
		if (controls_conf_map["Up"] == "Delete")
			Up = sf::Keyboard::Delete;
		if (controls_conf_map["Up"] == "PageUp")
			Up = sf::Keyboard::PageUp;
		if (controls_conf_map["Up"] == "PageDown")
			Up = sf::Keyboard::PageDown;
		if (controls_conf_map["Up"] == "Divide")
			Up = sf::Keyboard::Divide;
		if (controls_conf_map["Up"] == "Multiply")
			Up = sf::Keyboard::Multiply;
		if (controls_conf_map["Up"] == "Subtract")
			Up = sf::Keyboard::Subtract;
		if (controls_conf_map["Up"] == "Add")
			Up = sf::Keyboard::Add;
		if (controls_conf_map["Up"] == "LSystem")
			Up = sf::Keyboard::LSystem;
		if (controls_conf_map["Up"] == "RSystem")
			Up = sf::Keyboard::RSystem;
		if (controls_conf_map["Up"] == "Menu")
			Up = sf::Keyboard::Menu;
		if (controls_conf_map["Up"] == "Insert")
			Up = sf::Keyboard::Insert;
		if (controls_conf_map["Up"] == "Pause")
			Up = sf::Keyboard::Pause;
	}

	if (controls_conf_map.find("Down") != controls_conf_map.end())
	{
		if (controls_conf_map["Down"] == "A")
			Down = sf::Keyboard::A;
		if (controls_conf_map["Down"] == "B")
			Down = sf::Keyboard::B;
		if (controls_conf_map["Down"] == "C")
			Down = sf::Keyboard::C;
		if (controls_conf_map["Down"] == "D")
			Down = sf::Keyboard::D;
		if (controls_conf_map["Down"] == "E")
			Down = sf::Keyboard::E;
		if (controls_conf_map["Down"] == "F")
			Down = sf::Keyboard::F;
		if (controls_conf_map["Down"] == "G")
			Down = sf::Keyboard::G;
		if (controls_conf_map["Down"] == "H")
			Down = sf::Keyboard::H;
		if (controls_conf_map["Down"] == "I")
			Down = sf::Keyboard::I;
		if (controls_conf_map["Down"] == "J")
			Down = sf::Keyboard::J;
		if (controls_conf_map["Down"] == "K")
			Down = sf::Keyboard::K;
		if (controls_conf_map["Down"] == "L")
			Down = sf::Keyboard::L;
		if (controls_conf_map["Down"] == "M")
			Down = sf::Keyboard::M;
		if (controls_conf_map["Down"] == "N")
			Down = sf::Keyboard::N;
		if (controls_conf_map["Down"] == "O")
			Down = sf::Keyboard::O;
		if (controls_conf_map["Down"] == "P")
			Down = sf::Keyboard::P;
		if (controls_conf_map["Down"] == "Q")
			Down = sf::Keyboard::Q;
		if (controls_conf_map["Down"] == "R")
			Down = sf::Keyboard::R;
		if (controls_conf_map["Down"] == "S")
			Down = sf::Keyboard::S;
		if (controls_conf_map["Down"] == "T")
			Down = sf::Keyboard::T;
		if (controls_conf_map["Down"] == "U")
			Down = sf::Keyboard::U;
		if (controls_conf_map["Down"] == "V")
			Down = sf::Keyboard::V;
		if (controls_conf_map["Down"] == "W")
			Down = sf::Keyboard::W;
		if (controls_conf_map["Down"] == "X")
			Down = sf::Keyboard::X;
		if (controls_conf_map["Down"] == "Y")
			Down = sf::Keyboard::Y;
		if (controls_conf_map["Down"] == "Z")
			Down = sf::Keyboard::Z;
		if (controls_conf_map["Down"] == "Num0")
			Down = sf::Keyboard::Num0;
		if (controls_conf_map["Down"] == "Numpad0")
			Down = sf::Keyboard::Numpad0;
		if (controls_conf_map["Down"] == "Num1")
			Down = sf::Keyboard::Num1;
		if (controls_conf_map["Down"] == "Numpad1")
			Down = sf::Keyboard::Numpad1;
		if (controls_conf_map["Down"] == "Num2")
			Down = sf::Keyboard::Num2;
		if (controls_conf_map["Down"] == "Numpad2")
			Down = sf::Keyboard::Numpad2;
		if (controls_conf_map["Down"] == "Num3")
			Down = sf::Keyboard::Num3;
		if (controls_conf_map["Down"] == "Numpad3")
			Down = sf::Keyboard::Numpad3;
		if (controls_conf_map["Down"] == "Num4")
			Down = sf::Keyboard::Num4;
		if (controls_conf_map["Down"] == "Numpad4")
			Down = sf::Keyboard::Numpad4;
		if (controls_conf_map["Down"] == "Num5")
			Down = sf::Keyboard::Num5;
		if (controls_conf_map["Down"] == "Numpad5")
			Down = sf::Keyboard::Numpad5;
		if (controls_conf_map["Down"] == "Num6")
			Down = sf::Keyboard::Num6;
		if (controls_conf_map["Down"] == "Numpad6")
			Down = sf::Keyboard::Numpad6;
		if (controls_conf_map["Down"] == "Num7")
			Down = sf::Keyboard::Num7;
		if (controls_conf_map["Down"] == "Numpad7")
			Down = sf::Keyboard::Numpad7;
		if (controls_conf_map["Down"] == "Num8")
			Down = sf::Keyboard::Num8;
		if (controls_conf_map["Down"] == "Numpad8")
			Down = sf::Keyboard::Numpad8;
		if (controls_conf_map["Down"] == "Num9")
			Down = sf::Keyboard::Num9;
		if (controls_conf_map["Down"] == "Numpad9")
			Down = sf::Keyboard::Numpad9;
		if (controls_conf_map["Down"] == "F1")
			Down = sf::Keyboard::F1;
		if (controls_conf_map["Down"] == "F2")
			Down = sf::Keyboard::F2;
		if (controls_conf_map["Down"] == "F3")
			Down = sf::Keyboard::F3;
		if (controls_conf_map["Down"] == "F4")
			Down = sf::Keyboard::F4;
		if (controls_conf_map["Down"] == "F5")
			Down = sf::Keyboard::F5;
		if (controls_conf_map["Down"] == "F6")
			Down = sf::Keyboard::F6;
		if (controls_conf_map["Down"] == "F7")
			Down = sf::Keyboard::F7;
		if (controls_conf_map["Down"] == "F8")
			Down = sf::Keyboard::F8;
		if (controls_conf_map["Down"] == "F9")
			Down = sf::Keyboard::F9;
		if (controls_conf_map["Down"] == "F10")
			Down = sf::Keyboard::F10;
		if (controls_conf_map["Down"] == "F11")
			Down = sf::Keyboard::F11;
		if (controls_conf_map["Down"] == "F12")
			Down = sf::Keyboard::F12;
		if (controls_conf_map["Down"] == "Space")
			Down = sf::Keyboard::Space;
		if (controls_conf_map["Down"] == "Left")
			Down = sf::Keyboard::Left;
		if (controls_conf_map["Down"] == "Right")
			Down = sf::Keyboard::Right;
		if (controls_conf_map["Down"] == "Up")
			Down = sf::Keyboard::Up;
		if (controls_conf_map["Down"] == "Down")
			Down = sf::Keyboard::Down;
		if (controls_conf_map["Down"] == "Dash")
			Down = sf::Keyboard::Dash;
		if (controls_conf_map["Down"] == "Equal")
			Down = sf::Keyboard::Equal;
		if (controls_conf_map["Down"] == "BackSpace")
			#ifdef COMPILING_FOR_WINDOWS
			Down = sf::Keyboard::Back;
			#else
			Down = sf::Keyboard::BackSpace;
			#endif
		if (controls_conf_map["Down"] == "Tab")
			Down = sf::Keyboard::Tab;
		if (controls_conf_map["Down"] == "SemiColon")
			Down = sf::Keyboard::SemiColon;
		if (controls_conf_map["Down"] == "Quote")
			Down = sf::Keyboard::Quote;
		if (controls_conf_map["Down"] == "Comma")
			Down = sf::Keyboard::Comma;
		if (controls_conf_map["Down"] == "Period")
			Down = sf::Keyboard::Period;
		if (controls_conf_map["Down"] == "Slash")
			Down = sf::Keyboard::Slash;
		if (controls_conf_map["Down"] == "LBracket")
			Down = sf::Keyboard::LBracket;
		if (controls_conf_map["Down"] == "RBracket")
			Down = sf::Keyboard::RBracket;
		if (controls_conf_map["Down"] == "BackSlash")
			Down = sf::Keyboard::BackSlash;
		if (controls_conf_map["Down"] == "Return")
			Down = sf::Keyboard::Return;
		if (controls_conf_map["Down"] == "LShift")
			Down = sf::Keyboard::LShift;
		if (controls_conf_map["Down"] == "RShift")
			Down = sf::Keyboard::RShift;
		if (controls_conf_map["Down"] == "LControl")
			Down = sf::Keyboard::LControl;
		if (controls_conf_map["Down"] == "RControl")
			Down = sf::Keyboard::RControl;
		if (controls_conf_map["Down"] == "LAlt")
			Down = sf::Keyboard::LAlt;
		if (controls_conf_map["Down"] == "RAlt")
			Down = sf::Keyboard::RAlt;
		if (controls_conf_map["Down"] == "Home")
			Down = sf::Keyboard::Home;
		if (controls_conf_map["Down"] == "End")
			Down = sf::Keyboard::End;
		if (controls_conf_map["Down"] == "Delete")
			Down = sf::Keyboard::Delete;
		if (controls_conf_map["Down"] == "PageUp")
			Down = sf::Keyboard::PageUp;
		if (controls_conf_map["Down"] == "PageDown")
			Down = sf::Keyboard::PageDown;
		if (controls_conf_map["Down"] == "Divide")
			Down = sf::Keyboard::Divide;
		if (controls_conf_map["Down"] == "Multiply")
			Down = sf::Keyboard::Multiply;
		if (controls_conf_map["Down"] == "Subtract")
			Down = sf::Keyboard::Subtract;
		if (controls_conf_map["Down"] == "Add")
			Down = sf::Keyboard::Add;
		if (controls_conf_map["Down"] == "LSystem")
			Down = sf::Keyboard::LSystem;
		if (controls_conf_map["Down"] == "RSystem")
			Down = sf::Keyboard::RSystem;
		if (controls_conf_map["Down"] == "Menu")
			Down = sf::Keyboard::Menu;
		if (controls_conf_map["Down"] == "Insert")
			Down = sf::Keyboard::Insert;
		if (controls_conf_map["Down"] == "Pause")
			Down = sf::Keyboard::Pause;
	}

	if (controls_conf_map.find("Jump") != controls_conf_map.end())
	{
		if (controls_conf_map["Jump"] == "A")
			Jump = sf::Keyboard::A;
		if (controls_conf_map["Jump"] == "B")
			Jump = sf::Keyboard::B;
		if (controls_conf_map["Jump"] == "C")
			Jump = sf::Keyboard::C;
		if (controls_conf_map["Jump"] == "D")
			Jump = sf::Keyboard::D;
		if (controls_conf_map["Jump"] == "E")
			Jump = sf::Keyboard::E;
		if (controls_conf_map["Jump"] == "F")
			Jump = sf::Keyboard::F;
		if (controls_conf_map["Jump"] == "G")
			Jump = sf::Keyboard::G;
		if (controls_conf_map["Jump"] == "H")
			Jump = sf::Keyboard::H;
		if (controls_conf_map["Jump"] == "I")
			Jump = sf::Keyboard::I;
		if (controls_conf_map["Jump"] == "J")
			Jump = sf::Keyboard::J;
		if (controls_conf_map["Jump"] == "K")
			Jump = sf::Keyboard::K;
		if (controls_conf_map["Jump"] == "L")
			Jump = sf::Keyboard::L;
		if (controls_conf_map["Jump"] == "M")
			Jump = sf::Keyboard::M;
		if (controls_conf_map["Jump"] == "N")
			Jump = sf::Keyboard::N;
		if (controls_conf_map["Jump"] == "O")
			Jump = sf::Keyboard::O;
		if (controls_conf_map["Jump"] == "P")
			Jump = sf::Keyboard::P;
		if (controls_conf_map["Jump"] == "Q")
			Jump = sf::Keyboard::Q;
		if (controls_conf_map["Jump"] == "R")
			Jump = sf::Keyboard::R;
		if (controls_conf_map["Jump"] == "S")
			Jump = sf::Keyboard::S;
		if (controls_conf_map["Jump"] == "T")
			Jump = sf::Keyboard::T;
		if (controls_conf_map["Jump"] == "U")
			Jump = sf::Keyboard::U;
		if (controls_conf_map["Jump"] == "V")
			Jump = sf::Keyboard::V;
		if (controls_conf_map["Jump"] == "W")
			Jump = sf::Keyboard::W;
		if (controls_conf_map["Jump"] == "X")
			Jump = sf::Keyboard::X;
		if (controls_conf_map["Jump"] == "Y")
			Jump = sf::Keyboard::Y;
		if (controls_conf_map["Jump"] == "Z")
			Jump = sf::Keyboard::Z;
		if (controls_conf_map["Jump"] == "Num0")
			Jump = sf::Keyboard::Num0;
		if (controls_conf_map["Jump"] == "Numpad0")
			Jump = sf::Keyboard::Numpad0;
		if (controls_conf_map["Jump"] == "Num1")
			Jump = sf::Keyboard::Num1;
		if (controls_conf_map["Jump"] == "Numpad1")
			Jump = sf::Keyboard::Numpad1;
		if (controls_conf_map["Jump"] == "Num2")
			Jump = sf::Keyboard::Num2;
		if (controls_conf_map["Jump"] == "Numpad2")
			Jump = sf::Keyboard::Numpad2;
		if (controls_conf_map["Jump"] == "Num3")
			Jump = sf::Keyboard::Num3;
		if (controls_conf_map["Jump"] == "Numpad3")
			Jump = sf::Keyboard::Numpad3;
		if (controls_conf_map["Jump"] == "Num4")
			Jump = sf::Keyboard::Num4;
		if (controls_conf_map["Jump"] == "Numpad4")
			Jump = sf::Keyboard::Numpad4;
		if (controls_conf_map["Jump"] == "Num5")
			Jump = sf::Keyboard::Num5;
		if (controls_conf_map["Jump"] == "Numpad5")
			Jump = sf::Keyboard::Numpad5;
		if (controls_conf_map["Jump"] == "Num6")
			Jump = sf::Keyboard::Num6;
		if (controls_conf_map["Jump"] == "Numpad6")
			Jump = sf::Keyboard::Numpad6;
		if (controls_conf_map["Jump"] == "Num7")
			Jump = sf::Keyboard::Num7;
		if (controls_conf_map["Jump"] == "Numpad7")
			Jump = sf::Keyboard::Numpad7;
		if (controls_conf_map["Jump"] == "Num8")
			Jump = sf::Keyboard::Num8;
		if (controls_conf_map["Jump"] == "Numpad8")
			Jump = sf::Keyboard::Numpad8;
		if (controls_conf_map["Jump"] == "Num9")
			Jump = sf::Keyboard::Num9;
		if (controls_conf_map["Jump"] == "Numpad9")
			Jump = sf::Keyboard::Numpad9;
		if (controls_conf_map["Jump"] == "F1")
			Jump = sf::Keyboard::F1;
		if (controls_conf_map["Jump"] == "F2")
			Jump = sf::Keyboard::F2;
		if (controls_conf_map["Jump"] == "F3")
			Jump = sf::Keyboard::F3;
		if (controls_conf_map["Jump"] == "F4")
			Jump = sf::Keyboard::F4;
		if (controls_conf_map["Jump"] == "F5")
			Jump = sf::Keyboard::F5;
		if (controls_conf_map["Jump"] == "F6")
			Jump = sf::Keyboard::F6;
		if (controls_conf_map["Jump"] == "F7")
			Jump = sf::Keyboard::F7;
		if (controls_conf_map["Jump"] == "F8")
			Jump = sf::Keyboard::F8;
		if (controls_conf_map["Jump"] == "F9")
			Jump = sf::Keyboard::F9;
		if (controls_conf_map["Jump"] == "F10")
			Jump = sf::Keyboard::F10;
		if (controls_conf_map["Jump"] == "F11")
			Jump = sf::Keyboard::F11;
		if (controls_conf_map["Jump"] == "F12")
			Jump = sf::Keyboard::F12;
		if (controls_conf_map["Jump"] == "Space")
			Jump = sf::Keyboard::Space;
		if (controls_conf_map["Jump"] == "Left")
			Jump = sf::Keyboard::Left;
		if (controls_conf_map["Jump"] == "Right")
			Jump = sf::Keyboard::Right;
		if (controls_conf_map["Jump"] == "Up")
			Jump = sf::Keyboard::Up;
		if (controls_conf_map["Jump"] == "Down")
			Jump = sf::Keyboard::Down;
		if (controls_conf_map["Jump"] == "Dash")
			Jump = sf::Keyboard::Dash;
		if (controls_conf_map["Jump"] == "Equal")
			Jump = sf::Keyboard::Equal;
		if (controls_conf_map["Jump"] == "BackSpace")
			#ifdef COMPILING_FOR_WINDOWS
			Jump = sf::Keyboard::Back;
			#else
			Jump = sf::Keyboard::BackSpace;
			#endif
		if (controls_conf_map["Jump"] == "Tab")
			Jump = sf::Keyboard::Tab;
		if (controls_conf_map["Jump"] == "SemiColon")
			Jump = sf::Keyboard::SemiColon;
		if (controls_conf_map["Jump"] == "Quote")
			Jump = sf::Keyboard::Quote;
		if (controls_conf_map["Jump"] == "Comma")
			Jump = sf::Keyboard::Comma;
		if (controls_conf_map["Jump"] == "Period")
			Jump = sf::Keyboard::Period;
		if (controls_conf_map["Jump"] == "Slash")
			Jump = sf::Keyboard::Slash;
		if (controls_conf_map["Jump"] == "LBracket")
			Jump = sf::Keyboard::LBracket;
		if (controls_conf_map["Jump"] == "RBracket")
			Jump = sf::Keyboard::RBracket;
		if (controls_conf_map["Jump"] == "BackSlash")
			Jump = sf::Keyboard::BackSlash;
		if (controls_conf_map["Jump"] == "Return")
			Jump = sf::Keyboard::Return;
		if (controls_conf_map["Jump"] == "LShift")
			Jump = sf::Keyboard::LShift;
		if (controls_conf_map["Jump"] == "RShift")
			Jump = sf::Keyboard::RShift;
		if (controls_conf_map["Jump"] == "LControl")
			Jump = sf::Keyboard::LControl;
		if (controls_conf_map["Jump"] == "RControl")
			Jump = sf::Keyboard::RControl;
		if (controls_conf_map["Jump"] == "LAlt")
			Jump = sf::Keyboard::LAlt;
		if (controls_conf_map["Jump"] == "RAlt")
			Jump = sf::Keyboard::RAlt;
		if (controls_conf_map["Jump"] == "Home")
			Jump = sf::Keyboard::Home;
		if (controls_conf_map["Jump"] == "End")
			Jump = sf::Keyboard::End;
		if (controls_conf_map["Jump"] == "Delete")
			Jump = sf::Keyboard::Delete;
		if (controls_conf_map["Jump"] == "PageUp")
			Jump = sf::Keyboard::PageUp;
		if (controls_conf_map["Jump"] == "PageDown")
			Jump = sf::Keyboard::PageDown;
		if (controls_conf_map["Jump"] == "Divide")
			Jump = sf::Keyboard::Divide;
		if (controls_conf_map["Jump"] == "Multiply")
			Jump = sf::Keyboard::Multiply;
		if (controls_conf_map["Jump"] == "Subtract")
			Jump = sf::Keyboard::Subtract;
		if (controls_conf_map["Jump"] == "Add")
			Jump = sf::Keyboard::Add;
		if (controls_conf_map["Jump"] == "LSystem")
			Jump = sf::Keyboard::LSystem;
		if (controls_conf_map["Jump"] == "RSystem")
			Jump = sf::Keyboard::RSystem;
		if (controls_conf_map["Jump"] == "Menu")
			Jump = sf::Keyboard::Menu;
		if (controls_conf_map["Jump"] == "Insert")
			Jump = sf::Keyboard::Insert;
		if (controls_conf_map["Jump"] == "Pause")
			Jump = sf::Keyboard::Pause;
	}

	if (controls_conf_map.find("Rocketpack") != controls_conf_map.end())
	{
		if (controls_conf_map["Rocketpack"] == "A")
			Rocketpack = sf::Keyboard::A;
		if (controls_conf_map["Rocketpack"] == "B")
			Rocketpack = sf::Keyboard::B;
		if (controls_conf_map["Rocketpack"] == "C")
			Rocketpack = sf::Keyboard::C;
		if (controls_conf_map["Rocketpack"] == "D")
			Rocketpack = sf::Keyboard::D;
		if (controls_conf_map["Rocketpack"] == "E")
			Rocketpack = sf::Keyboard::E;
		if (controls_conf_map["Rocketpack"] == "F")
			Rocketpack = sf::Keyboard::F;
		if (controls_conf_map["Rocketpack"] == "G")
			Rocketpack = sf::Keyboard::G;
		if (controls_conf_map["Rocketpack"] == "H")
			Rocketpack = sf::Keyboard::H;
		if (controls_conf_map["Rocketpack"] == "I")
			Rocketpack = sf::Keyboard::I;
		if (controls_conf_map["Rocketpack"] == "J")
			Rocketpack = sf::Keyboard::J;
		if (controls_conf_map["Rocketpack"] == "K")
			Rocketpack = sf::Keyboard::K;
		if (controls_conf_map["Rocketpack"] == "L")
			Rocketpack = sf::Keyboard::L;
		if (controls_conf_map["Rocketpack"] == "M")
			Rocketpack = sf::Keyboard::M;
		if (controls_conf_map["Rocketpack"] == "N")
			Rocketpack = sf::Keyboard::N;
		if (controls_conf_map["Rocketpack"] == "O")
			Rocketpack = sf::Keyboard::O;
		if (controls_conf_map["Rocketpack"] == "P")
			Rocketpack = sf::Keyboard::P;
		if (controls_conf_map["Rocketpack"] == "Q")
			Rocketpack = sf::Keyboard::Q;
		if (controls_conf_map["Rocketpack"] == "R")
			Rocketpack = sf::Keyboard::R;
		if (controls_conf_map["Rocketpack"] == "S")
			Rocketpack = sf::Keyboard::S;
		if (controls_conf_map["Rocketpack"] == "T")
			Rocketpack = sf::Keyboard::T;
		if (controls_conf_map["Rocketpack"] == "U")
			Rocketpack = sf::Keyboard::U;
		if (controls_conf_map["Rocketpack"] == "V")
			Rocketpack = sf::Keyboard::V;
		if (controls_conf_map["Rocketpack"] == "W")
			Rocketpack = sf::Keyboard::W;
		if (controls_conf_map["Rocketpack"] == "X")
			Rocketpack = sf::Keyboard::X;
		if (controls_conf_map["Rocketpack"] == "Y")
			Rocketpack = sf::Keyboard::Y;
		if (controls_conf_map["Rocketpack"] == "Z")
			Rocketpack = sf::Keyboard::Z;
		if (controls_conf_map["Rocketpack"] == "Num0")
			Rocketpack = sf::Keyboard::Num0;
		if (controls_conf_map["Rocketpack"] == "Numpad0")
			Rocketpack = sf::Keyboard::Numpad0;
		if (controls_conf_map["Rocketpack"] == "Num1")
			Rocketpack = sf::Keyboard::Num1;
		if (controls_conf_map["Rocketpack"] == "Numpad1")
			Rocketpack = sf::Keyboard::Numpad1;
		if (controls_conf_map["Rocketpack"] == "Num2")
			Rocketpack = sf::Keyboard::Num2;
		if (controls_conf_map["Rocketpack"] == "Numpad2")
			Rocketpack = sf::Keyboard::Numpad2;
		if (controls_conf_map["Rocketpack"] == "Num3")
			Rocketpack = sf::Keyboard::Num3;
		if (controls_conf_map["Rocketpack"] == "Numpad3")
			Rocketpack = sf::Keyboard::Numpad3;
		if (controls_conf_map["Rocketpack"] == "Num4")
			Rocketpack = sf::Keyboard::Num4;
		if (controls_conf_map["Rocketpack"] == "Numpad4")
			Rocketpack = sf::Keyboard::Numpad4;
		if (controls_conf_map["Rocketpack"] == "Num5")
			Rocketpack = sf::Keyboard::Num5;
		if (controls_conf_map["Rocketpack"] == "Numpad5")
			Rocketpack = sf::Keyboard::Numpad5;
		if (controls_conf_map["Rocketpack"] == "Num6")
			Rocketpack = sf::Keyboard::Num6;
		if (controls_conf_map["Rocketpack"] == "Numpad6")
			Rocketpack = sf::Keyboard::Numpad6;
		if (controls_conf_map["Rocketpack"] == "Num7")
			Rocketpack = sf::Keyboard::Num7;
		if (controls_conf_map["Rocketpack"] == "Numpad7")
			Rocketpack = sf::Keyboard::Numpad7;
		if (controls_conf_map["Rocketpack"] == "Num8")
			Rocketpack = sf::Keyboard::Num8;
		if (controls_conf_map["Rocketpack"] == "Numpad8")
			Rocketpack = sf::Keyboard::Numpad8;
		if (controls_conf_map["Rocketpack"] == "Num9")
			Rocketpack = sf::Keyboard::Num9;
		if (controls_conf_map["Rocketpack"] == "Numpad9")
			Rocketpack = sf::Keyboard::Numpad9;
		if (controls_conf_map["Rocketpack"] == "F1")
			Rocketpack = sf::Keyboard::F1;
		if (controls_conf_map["Rocketpack"] == "F2")
			Rocketpack = sf::Keyboard::F2;
		if (controls_conf_map["Rocketpack"] == "F3")
			Rocketpack = sf::Keyboard::F3;
		if (controls_conf_map["Rocketpack"] == "F4")
			Rocketpack = sf::Keyboard::F4;
		if (controls_conf_map["Rocketpack"] == "F5")
			Rocketpack = sf::Keyboard::F5;
		if (controls_conf_map["Rocketpack"] == "F6")
			Rocketpack = sf::Keyboard::F6;
		if (controls_conf_map["Rocketpack"] == "F7")
			Rocketpack = sf::Keyboard::F7;
		if (controls_conf_map["Rocketpack"] == "F8")
			Rocketpack = sf::Keyboard::F8;
		if (controls_conf_map["Rocketpack"] == "F9")
			Rocketpack = sf::Keyboard::F9;
		if (controls_conf_map["Rocketpack"] == "F10")
			Rocketpack = sf::Keyboard::F10;
		if (controls_conf_map["Rocketpack"] == "F11")
			Rocketpack = sf::Keyboard::F11;
		if (controls_conf_map["Rocketpack"] == "F12")
			Rocketpack = sf::Keyboard::F12;
		if (controls_conf_map["Rocketpack"] == "Space")
			Rocketpack = sf::Keyboard::Space;
		if (controls_conf_map["Rocketpack"] == "Left")
			Rocketpack = sf::Keyboard::Left;
		if (controls_conf_map["Rocketpack"] == "Right")
			Rocketpack = sf::Keyboard::Right;
		if (controls_conf_map["Rocketpack"] == "Up")
			Rocketpack = sf::Keyboard::Up;
		if (controls_conf_map["Rocketpack"] == "Down")
			Rocketpack = sf::Keyboard::Down;
		if (controls_conf_map["Rocketpack"] == "Dash")
			Rocketpack = sf::Keyboard::Dash;
		if (controls_conf_map["Rocketpack"] == "Equal")
			Rocketpack = sf::Keyboard::Equal;
		if (controls_conf_map["Rocketpack"] == "BackSpace")
			#ifdef COMPILING_FOR_WINDOWS
			Rocketpack = sf::Keyboard::Back;
			#else
			Rocketpack = sf::Keyboard::BackSpace;
			#endif
		if (controls_conf_map["Rocketpack"] == "Tab")
			Rocketpack = sf::Keyboard::Tab;
		if (controls_conf_map["Rocketpack"] == "SemiColon")
			Rocketpack = sf::Keyboard::SemiColon;
		if (controls_conf_map["Rocketpack"] == "Quote")
			Rocketpack = sf::Keyboard::Quote;
		if (controls_conf_map["Rocketpack"] == "Comma")
			Rocketpack = sf::Keyboard::Comma;
		if (controls_conf_map["Rocketpack"] == "Period")
			Rocketpack = sf::Keyboard::Period;
		if (controls_conf_map["Rocketpack"] == "Slash")
			Rocketpack = sf::Keyboard::Slash;
		if (controls_conf_map["Rocketpack"] == "LBracket")
			Rocketpack = sf::Keyboard::LBracket;
		if (controls_conf_map["Rocketpack"] == "RBracket")
			Rocketpack = sf::Keyboard::RBracket;
		if (controls_conf_map["Rocketpack"] == "BackSlash")
			Rocketpack = sf::Keyboard::BackSlash;
		if (controls_conf_map["Rocketpack"] == "Return")
			Rocketpack = sf::Keyboard::Return;
		if (controls_conf_map["Rocketpack"] == "LShift")
			Rocketpack = sf::Keyboard::LShift;
		if (controls_conf_map["Rocketpack"] == "RShift")
			Rocketpack = sf::Keyboard::RShift;
		if (controls_conf_map["Rocketpack"] == "LControl")
			Rocketpack = sf::Keyboard::LControl;
		if (controls_conf_map["Rocketpack"] == "RControl")
			Rocketpack = sf::Keyboard::RControl;
		if (controls_conf_map["Rocketpack"] == "LAlt")
			Rocketpack = sf::Keyboard::LAlt;
		if (controls_conf_map["Rocketpack"] == "RAlt")
			Rocketpack = sf::Keyboard::RAlt;
		if (controls_conf_map["Rocketpack"] == "Home")
			Rocketpack = sf::Keyboard::Home;
		if (controls_conf_map["Rocketpack"] == "End")
			Rocketpack = sf::Keyboard::End;
		if (controls_conf_map["Rocketpack"] == "Delete")
			Rocketpack = sf::Keyboard::Delete;
		if (controls_conf_map["Rocketpack"] == "PageUp")
			Rocketpack = sf::Keyboard::PageUp;
		if (controls_conf_map["Rocketpack"] == "PageDown")
			Rocketpack = sf::Keyboard::PageDown;
		if (controls_conf_map["Rocketpack"] == "Divide")
			Rocketpack = sf::Keyboard::Divide;
		if (controls_conf_map["Rocketpack"] == "Multiply")
			Rocketpack = sf::Keyboard::Multiply;
		if (controls_conf_map["Rocketpack"] == "Subtract")
			Rocketpack = sf::Keyboard::Subtract;
		if (controls_conf_map["Rocketpack"] == "Add")
			Rocketpack = sf::Keyboard::Add;
		if (controls_conf_map["Rocketpack"] == "LSystem")
			Rocketpack = sf::Keyboard::LSystem;
		if (controls_conf_map["Rocketpack"] == "RSystem")
			Rocketpack = sf::Keyboard::RSystem;
		if (controls_conf_map["Rocketpack"] == "Menu")
			Rocketpack = sf::Keyboard::Menu;
		if (controls_conf_map["Rocketpack"] == "Insert")
			Rocketpack = sf::Keyboard::Insert;
		if (controls_conf_map["Rocketpack"] == "Pause")
			Rocketpack = sf::Keyboard::Pause;
	}

	if (controls_conf_map.find("Pause") != controls_conf_map.end())
	{
		if (controls_conf_map["Pause"] == "A")
			Pause = sf::Keyboard::A;
		if (controls_conf_map["Pause"] == "B")
			Pause = sf::Keyboard::B;
		if (controls_conf_map["Pause"] == "C")
			Pause = sf::Keyboard::C;
		if (controls_conf_map["Pause"] == "D")
			Pause = sf::Keyboard::D;
		if (controls_conf_map["Pause"] == "E")
			Pause = sf::Keyboard::E;
		if (controls_conf_map["Pause"] == "F")
			Pause = sf::Keyboard::F;
		if (controls_conf_map["Pause"] == "G")
			Pause = sf::Keyboard::G;
		if (controls_conf_map["Pause"] == "H")
			Pause = sf::Keyboard::H;
		if (controls_conf_map["Pause"] == "I")
			Pause = sf::Keyboard::I;
		if (controls_conf_map["Pause"] == "J")
			Pause = sf::Keyboard::J;
		if (controls_conf_map["Pause"] == "K")
			Pause = sf::Keyboard::K;
		if (controls_conf_map["Pause"] == "L")
			Pause = sf::Keyboard::L;
		if (controls_conf_map["Pause"] == "M")
			Pause = sf::Keyboard::M;
		if (controls_conf_map["Pause"] == "N")
			Pause = sf::Keyboard::N;
		if (controls_conf_map["Pause"] == "O")
			Pause = sf::Keyboard::O;
		if (controls_conf_map["Pause"] == "P")
			Pause = sf::Keyboard::P;
		if (controls_conf_map["Pause"] == "Q")
			Pause = sf::Keyboard::Q;
		if (controls_conf_map["Pause"] == "R")
			Pause = sf::Keyboard::R;
		if (controls_conf_map["Pause"] == "S")
			Pause = sf::Keyboard::S;
		if (controls_conf_map["Pause"] == "T")
			Pause = sf::Keyboard::T;
		if (controls_conf_map["Pause"] == "U")
			Pause = sf::Keyboard::U;
		if (controls_conf_map["Pause"] == "V")
			Pause = sf::Keyboard::V;
		if (controls_conf_map["Pause"] == "W")
			Pause = sf::Keyboard::W;
		if (controls_conf_map["Pause"] == "X")
			Pause = sf::Keyboard::X;
		if (controls_conf_map["Pause"] == "Y")
			Pause = sf::Keyboard::Y;
		if (controls_conf_map["Pause"] == "Z")
			Pause = sf::Keyboard::Z;
		if (controls_conf_map["Pause"] == "Num0")
			Pause = sf::Keyboard::Num0;
		if (controls_conf_map["Pause"] == "Numpad0")
			Pause = sf::Keyboard::Numpad0;
		if (controls_conf_map["Pause"] == "Num1")
			Pause = sf::Keyboard::Num1;
		if (controls_conf_map["Pause"] == "Numpad1")
			Pause = sf::Keyboard::Numpad1;
		if (controls_conf_map["Pause"] == "Num2")
			Pause = sf::Keyboard::Num2;
		if (controls_conf_map["Pause"] == "Numpad2")
			Pause = sf::Keyboard::Numpad2;
		if (controls_conf_map["Pause"] == "Num3")
			Pause = sf::Keyboard::Num3;
		if (controls_conf_map["Pause"] == "Numpad3")
			Pause = sf::Keyboard::Numpad3;
		if (controls_conf_map["Pause"] == "Num4")
			Pause = sf::Keyboard::Num4;
		if (controls_conf_map["Pause"] == "Numpad4")
			Pause = sf::Keyboard::Numpad4;
		if (controls_conf_map["Pause"] == "Num5")
			Pause = sf::Keyboard::Num5;
		if (controls_conf_map["Pause"] == "Numpad5")
			Pause = sf::Keyboard::Numpad5;
		if (controls_conf_map["Pause"] == "Num6")
			Pause = sf::Keyboard::Num6;
		if (controls_conf_map["Pause"] == "Numpad6")
			Pause = sf::Keyboard::Numpad6;
		if (controls_conf_map["Pause"] == "Num7")
			Pause = sf::Keyboard::Num7;
		if (controls_conf_map["Pause"] == "Numpad7")
			Pause = sf::Keyboard::Numpad7;
		if (controls_conf_map["Pause"] == "Num8")
			Pause = sf::Keyboard::Num8;
		if (controls_conf_map["Pause"] == "Numpad8")
			Pause = sf::Keyboard::Numpad8;
		if (controls_conf_map["Pause"] == "Num9")
			Pause = sf::Keyboard::Num9;
		if (controls_conf_map["Pause"] == "Numpad9")
			Pause = sf::Keyboard::Numpad9;
		if (controls_conf_map["Pause"] == "F1")
			Pause = sf::Keyboard::F1;
		if (controls_conf_map["Pause"] == "F2")
			Pause = sf::Keyboard::F2;
		if (controls_conf_map["Pause"] == "F3")
			Pause = sf::Keyboard::F3;
		if (controls_conf_map["Pause"] == "F4")
			Pause = sf::Keyboard::F4;
		if (controls_conf_map["Pause"] == "F5")
			Pause = sf::Keyboard::F5;
		if (controls_conf_map["Pause"] == "F6")
			Pause = sf::Keyboard::F6;
		if (controls_conf_map["Pause"] == "F7")
			Pause = sf::Keyboard::F7;
		if (controls_conf_map["Pause"] == "F8")
			Pause = sf::Keyboard::F8;
		if (controls_conf_map["Pause"] == "F9")
			Pause = sf::Keyboard::F9;
		if (controls_conf_map["Pause"] == "F10")
			Pause = sf::Keyboard::F10;
		if (controls_conf_map["Pause"] == "F11")
			Pause = sf::Keyboard::F11;
		if (controls_conf_map["Pause"] == "F12")
			Pause = sf::Keyboard::F12;
		if (controls_conf_map["Pause"] == "Space")
			Pause = sf::Keyboard::Space;
		if (controls_conf_map["Pause"] == "Left")
			Pause = sf::Keyboard::Left;
		if (controls_conf_map["Pause"] == "Right")
			Pause = sf::Keyboard::Right;
		if (controls_conf_map["Pause"] == "Up")
			Pause = sf::Keyboard::Up;
		if (controls_conf_map["Pause"] == "Down")
			Pause = sf::Keyboard::Down;
		if (controls_conf_map["Pause"] == "Dash")
			Pause = sf::Keyboard::Dash;
		if (controls_conf_map["Pause"] == "Equal")
			Pause = sf::Keyboard::Equal;
		if (controls_conf_map["Pause"] == "BackSpace")
			#ifdef COMPILING_FOR_WINDOWS
			Pause = sf::Keyboard::Back;
			#else
			Pause = sf::Keyboard::BackSpace;
			#endif
		if (controls_conf_map["Pause"] == "Tab")
			Pause = sf::Keyboard::Tab;
		if (controls_conf_map["Pause"] == "SemiColon")
			Pause = sf::Keyboard::SemiColon;
		if (controls_conf_map["Pause"] == "Quote")
			Pause = sf::Keyboard::Quote;
		if (controls_conf_map["Pause"] == "Comma")
			Pause = sf::Keyboard::Comma;
		if (controls_conf_map["Pause"] == "Period")
			Pause = sf::Keyboard::Period;
		if (controls_conf_map["Pause"] == "Slash")
			Pause = sf::Keyboard::Slash;
		if (controls_conf_map["Pause"] == "LBracket")
			Pause = sf::Keyboard::LBracket;
		if (controls_conf_map["Pause"] == "RBracket")
			Pause = sf::Keyboard::RBracket;
		if (controls_conf_map["Pause"] == "BackSlash")
			Pause = sf::Keyboard::BackSlash;
		if (controls_conf_map["Pause"] == "Return")
			Pause = sf::Keyboard::Return;
		if (controls_conf_map["Pause"] == "LShift")
			Pause = sf::Keyboard::LShift;
		if (controls_conf_map["Pause"] == "RShift")
			Pause = sf::Keyboard::RShift;
		if (controls_conf_map["Pause"] == "LControl")
			Pause = sf::Keyboard::LControl;
		if (controls_conf_map["Pause"] == "RControl")
			Pause = sf::Keyboard::RControl;
		if (controls_conf_map["Pause"] == "LAlt")
			Pause = sf::Keyboard::LAlt;
		if (controls_conf_map["Pause"] == "RAlt")
			Pause = sf::Keyboard::RAlt;
		if (controls_conf_map["Pause"] == "Home")
			Pause = sf::Keyboard::Home;
		if (controls_conf_map["Pause"] == "End")
			Pause = sf::Keyboard::End;
		if (controls_conf_map["Pause"] == "Delete")
			Pause = sf::Keyboard::Delete;
		if (controls_conf_map["Pause"] == "PageUp")
			Pause = sf::Keyboard::PageUp;
		if (controls_conf_map["Pause"] == "PageDown")
			Pause = sf::Keyboard::PageDown;
		if (controls_conf_map["Pause"] == "Divide")
			Pause = sf::Keyboard::Divide;
		if (controls_conf_map["Pause"] == "Multiply")
			Pause = sf::Keyboard::Multiply;
		if (controls_conf_map["Pause"] == "Subtract")
			Pause = sf::Keyboard::Subtract;
		if (controls_conf_map["Pause"] == "Add")
			Pause = sf::Keyboard::Add;
		if (controls_conf_map["Pause"] == "LSystem")
			Pause = sf::Keyboard::LSystem;
		if (controls_conf_map["Pause"] == "RSystem")
			Pause = sf::Keyboard::RSystem;
		if (controls_conf_map["Pause"] == "Menu")
			Pause = sf::Keyboard::Menu;
		if (controls_conf_map["Pause"] == "Insert")
			Pause = sf::Keyboard::Insert;
		if (controls_conf_map["Pause"] == "Pause")
			Pause = sf::Keyboard::Pause;
	}
}

Controls::~Controls()
{

}

}
