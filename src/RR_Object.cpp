/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * RR_Object.cpp
 *
 *  Created on: 2012-09-29
 *      Author: Jeremy Carter
 */

#include "../include/RR_Object.hpp"

namespace def
{

RR_Object::RR_Object(const std::string& model_filename, const std::string& texture_filename, glm::mat4& modelviewprojection_matrix)
	: def::Assimp_Resource(model_filename, texture_filename, modelviewprojection_matrix),
	  vert_shader_filename("shaders/default.vert"),
	  frag_shader_filename("shaders/default.frag")
{

}

RR_Object::RR_Object()
{

}

RR_Object::~RR_Object()
{

}

bool RR_Object::uses_default_vert_shader()
{
	if (vert_shader_filename != "shaders/default.vert")
		return false;

	return true;
}

bool RR_Object::uses_default_frag_shader()
{
	if (frag_shader_filename != "shaders/default.frag")
		return false;

	return true;
}

void RR_Object::set_vert_shader(const std::string& vertex_shader)
{

	vert_shader_filename = vertex_shader;
	//shaders.init(vert_shader_filename, frag_shader_filename);
	return;
}

void RR_Object::set_frag_shader(const std::string& fragment_shader)
{

	frag_shader_filename = fragment_shader;
	//shaders.init(vert_shader_filename, frag_shader_filename);
	return;
}

void RR_Object::init_shaders() /**  If you're overriding the defaults, you must
 	 	 	 	 	 	 	 	 	call the set_*_shader() functions before
 	 	 	 	 	 	 	 	 	calling this. */
{
	shaders.init(vert_shader_filename, frag_shader_filename);
}

}
