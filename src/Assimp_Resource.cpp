/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Assimp_Resource.cpp
 *
 *  Created on: 2012-06-21
 *      Author: Jeremy Carter
 */

#include "../include/Assimp_Resource.hpp"

namespace def
{

Assimp_Resource::Assimp_Resource(const std::string& model_filename, const std::string& texture_filename, glm::mat4& modelviewprojection_matrix)
	: _scene(0),
	  _modelviewprojection_matrix(modelviewprojection_matrix),
	  _position(0.0f, 0.0f, 0.0f, 1.0f),
	  _rotation(0.0),
	  num_meshes(0)
{

}

Assimp_Resource::Assimp_Resource()
	: _scene(0),
	  _position(0.0f, 0.0f, 0.0f, 1.0f),
	  _rotation(0.0),
	  num_meshes(0)
{

}

Assimp_Resource::~Assimp_Resource()
{

}

void Assimp_Resource::load(const std::string& model_filename, const std::string& texture_filename)
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	_scene = _importer.ReadFile(model_filename,
			 aiProcess_CalcTangentSpace		|
			 aiProcess_Triangulate				|
			 aiProcess_JoinIdenticalVertices	|
			 aiProcess_SortByPType );

	if (!_scene)
	{
		_scene = _importer.ReadFile(DATADIR"/"+model_filename,
				 aiProcess_CalcTangentSpace		|
				 aiProcess_Triangulate				|
				 aiProcess_JoinIdenticalVertices	|
				 aiProcess_SortByPType );

		if (!_scene)
		{
			std::cerr << "Assimp error: " << _importer.GetErrorString() << std::endl;
			throw exception_Assimp();
		}
	}

	num_meshes = _scene->mNumMeshes;

	vao_id.reset( new GLuint[num_meshes] );
	vbo_id.reset( new GLuint[num_meshes*2] );
	ibo_id.reset( new GLuint[num_meshes] );
	tex_id.reset( new GLuint[1] );
	uv_buffer.reset( new GLuint[1] );

	// store material colors
	aiColor4D mat_amb(0.0f, 0.0f, 0.0f, 0.0f);
	_scene->mMaterials[0]->Get(AI_MATKEY_COLOR_AMBIENT, mat_amb);
	_material_ambient = glm::vec4( mat_amb.r, mat_amb.g, mat_amb.b, mat_amb.a );

//	std::cout << std::endl << "material ambient: ( "
//			  << _material_ambient.r << ", "
//			  << _material_ambient.g << ", "
//			  << _material_ambient.b << " )" << std::endl;

	aiColor4D mat_diff(0.0f, 0.0f, 0.0f, 0.0f);
	_scene->mMaterials[0]->Get(AI_MATKEY_COLOR_DIFFUSE, mat_diff);
	_material_diffuse = glm::vec4( mat_diff.r, mat_diff.g, mat_diff.b, mat_diff.a );

//	std::cout << "material diffuse: ( "
//			  << _material_diffuse.r << ", "
//			  << _material_diffuse.g << ", "
//			  << _material_diffuse.b << " )" << std::endl;

	aiColor4D mat_spec(0.0f, 0.0f, 0.0f, 0.0f);
	_scene->mMaterials[0]->Get(AI_MATKEY_COLOR_SPECULAR, mat_spec);
	_material_specular = glm::vec4( mat_spec.r, mat_spec.g, mat_spec.b, mat_spec.a );

//	std::cout << "material specular: ( "
//			  << _material_specular.r << ", "
//			  << _material_specular.g << ", "
//			  << _material_specular.b << " )" << std::endl << std::endl;

	unsigned int vbo_index = 0;
	for (unsigned int i = 0; i < num_meshes; i++, vbo_index+=2)
	{
		aiMesh* mesh = _scene->mMeshes[i];

		// swap y and z axis to support collada exported from blender
		for (unsigned int j = 0; j < mesh->mNumVertices; j++)
		{
			float tmp = mesh->mVertices[j].y;
			mesh->mVertices[j].y = mesh->mVertices[j].z;
			mesh->mVertices[j].z = -tmp;

//			tmp = mesh->mVertices[j].x;
//			mesh->mVertices[j].x = mesh->mVertices[j].z;
//			mesh->mVertices[j].z = tmp;

			tmp = mesh->mNormals[j].y;
			mesh->mNormals[j].y = mesh->mNormals[j].z;
			mesh->mNormals[j].z = -tmp;

//			tmp = mesh->mNormals[j].x;
//			mesh->mNormals[j].x = mesh->mNormals[j].z;
//			mesh->mNormals[j].z = tmp;

//			float tmp3 = mesh->mTextureCoords[0][j].x;
//			mesh->mTextureCoords[0][j].x = mesh->mTextureCoords[0][j].y;
//			mesh->mTextureCoords[0][j].y = tmp3;
		}

		// convert assimp faces structure to OpenGL-compatible array
		std::unique_ptr< GLuint[] > faces( new GLuint[mesh->mNumFaces * sizeof(GLuint) * 3] );
		unsigned int face_index = 0;
		for (unsigned int j = 0; j < mesh->mNumFaces; j++)
		{
			std::memcpy(&faces[face_index], mesh->mFaces[j].mIndices, sizeof(GLfloat) * 3);
			face_index += 3;
		}

		glGenVertexArrays(1, &vao_id[i]); // generate VAO
		glBindVertexArray(vao_id[i]);	   // bind VAO

		glGenBuffers(1, &ibo_id[i]); // generate IBO
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_id[i]); // bind IBO
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * mesh->mNumFaces * 3, faces.get(), GL_STATIC_DRAW);

		// generate VBO for vertices
		if (mesh->HasPositions())
		{
			glGenBuffers(1, &vbo_id[vbo_index]);
			glBindBuffer(GL_ARRAY_BUFFER, vbo_id[vbo_index]);
//			glBufferData(GL_ARRAY_BUFFER, mesh->mNumVertices * sizeof(GLfloat) * 3, vertices.get(), GL_STATIC_DRAW);
			glBufferData(GL_ARRAY_BUFFER, mesh->mNumVertices * sizeof(GLfloat) * 3, (GLfloat*)mesh->mVertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray((GLuint)0);
			glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}

		// generate VBO for normals
		if (mesh->HasNormals())
		{
			glGenBuffers(1, &vbo_id[vbo_index+1]);
			glBindBuffer(GL_ARRAY_BUFFER, vbo_id[vbo_index+1]);
			glBufferData(GL_ARRAY_BUFFER, mesh->mNumVertices * sizeof(GLfloat) * 3, (GLfloat*)mesh->mNormals, GL_STATIC_DRAW);
			glEnableVertexAttribArray((GLuint)1);
			glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}

		if (mesh->HasTextureCoords(0))
		{
			sf::Image texture_image;

			// try to open file first to avoid assimp printing error to stdout
			std::string valid_texture_filename(texture_filename);
			std::ifstream texture_file(valid_texture_filename);
			if (!texture_file.is_open())
			{
				valid_texture_filename = DATADIR"/"+texture_filename;
				texture_file.open(valid_texture_filename);
				if (!texture_file.is_open())
				{
					throw exception_SFML();
				}
			}
			texture_file.close();
			texture_image.loadFromFile(valid_texture_filename);
			texture_image.flipVertically();

			// convert assimp TexCoords to OpenGL compatible array
			std::unique_ptr< GLfloat[] > tex_coords( new GLfloat[mesh->mNumVertices * 2] );
			for (unsigned int j = 0; j < mesh->mNumVertices; ++j)
			{
				tex_coords[j*2] = mesh->mTextureCoords[0][j].x;
				tex_coords[j*2+1] = mesh->mTextureCoords[0][j].y;
			}

			glGenTextures(1, &tex_id[0]);
			glBindTexture(GL_TEXTURE_2D, tex_id[0]);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_image.getSize().x, texture_image.getSize().y,
						 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_image.getPixelsPtr());
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glBindTexture(GL_TEXTURE_2D, 0);

			glGenBuffers(1, &uv_buffer[0]);
			glBindBuffer(GL_ARRAY_BUFFER, uv_buffer[0]);
			glBufferData(GL_ARRAY_BUFFER, mesh->mNumVertices * sizeof(GLfloat) * 2, tex_coords.get(), GL_STATIC_DRAW);
			glEnableVertexAttribArray((GLuint)2);
			glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		}

		// unbind buffers
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	}
	vbo_index = 0;
}

void Assimp_Resource::display(def::Shader& shaders)
{
	glEnable(GL_BLEND);
//	glDisable(GL_DEPTH_TEST);

	unsigned int vbo_index = 0;
	for (unsigned int i = 0; i < _scene->mNumMeshes; i++, vbo_index+=2)
	{
		_translation_matrix = glm::translate(_modelviewprojection_matrix,
											 glm::vec3(
												get_x_pos(),
												get_y_pos(),
												get_z_pos()));
		_rotation_matrix = glm::rotate(_modelviewprojection_matrix, (float)get_rot(), glm::vec3(0, 1, 0));

		GLint translation_matrix_location = glGetUniformLocation(shaders.id(), "translationMatrix");
		GLint rotation_matrix_location = glGetUniformLocation(shaders.id(), "rotationMatrix");
		GLint material_ambient_location = glGetUniformLocation(shaders.id(), "materialAmbient");
		GLint material_diffuse_location = glGetUniformLocation(shaders.id(), "materialDiffuse");
		GLint material_specular_location = glGetUniformLocation(shaders.id(), "materialSpecular");

		glUniformMatrix4fv(translation_matrix_location, 1, GL_FALSE, &_translation_matrix[0][0]);
		glUniformMatrix4fv(rotation_matrix_location, 1, GL_FALSE, &_rotation_matrix[0][0]);
		glUniform4fv(material_ambient_location, 1, &_material_ambient[0]);
		glUniform4fv(material_diffuse_location, 1, &_material_diffuse[0]);
		glUniform4fv(material_specular_location, 1, &_material_specular[0]);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tex_id[0]);

		// render mesh
		glBindVertexArray(vao_id[i]); // bind the VAO

		// draw the contents of the bound VAO
		glDrawElements(GL_TRIANGLES, _scene->mMeshes[i]->mNumFaces * 3, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0); // unbind the VAO

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	glDisable(GL_BLEND);
//	glEnable(GL_DEPTH_TEST);
}

void Assimp_Resource::set_pos(double x, double y, double z)
{
	_position.x = x;
	_position.y = y;
	_position.z = z;

	return;
}

void Assimp_Resource::set_x_pos(double x)
{
	_position.x = x;

	return;
}

void Assimp_Resource::set_y_pos(double y)
{
	_position.y = y;

	return;
}

void Assimp_Resource::set_z_pos(double z)
{
	_position.z = z;

	return;
}

void Assimp_Resource::set_rot(double degrees)
{
	_rotation = degrees;

	return;
}

double Assimp_Resource::get_x_pos()
{
	return _position.x;
}

double Assimp_Resource::get_y_pos()
{
	return _position.y;
}

double Assimp_Resource::get_z_pos()
{
	return _position.z;
}

double Assimp_Resource::get_rot()
{
	return _rotation;
}

}
