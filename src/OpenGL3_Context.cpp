/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * OpenGL3_Context.cpp
 *
 *  Created on: 2012-06-19
 *      Author: Jeremy Carter
 */

#include "../include/OpenGL3_Context.hpp"

namespace def
{

OpenGL3_Context::OpenGL3_Context( const int width,
								  const int height,
								  const bool fullscreen,
								  std::string title,
								  const int antialiasing_level,
								  const int major_version,
								  const int minor_version,
								  const int bpp,
								  const int stencil )
	: _window_title(title),
	  _window_bpp(bpp),
	  _context_settings(_window_bpp, stencil, antialiasing_level, major_version, minor_version),
	  window_width(width),
	  window_height(height)
{
	if (fullscreen)
		window.reset( new sf::Window(sf::VideoMode(window_width, window_height, _window_bpp), _window_title, sf::Style::Fullscreen, _context_settings) );
	else
		window.reset( new sf::Window(sf::VideoMode(window_width, window_height, _window_bpp), _window_title, sf::Style::Default, _context_settings) );

	window->setVerticalSyncEnabled(true);	// fixes jitters during motion
	//window->setFramerateLimit(60);
	window->setMouseCursorVisible(0);
	window->setActive();

	std::cout << "an OpenGL context has been created" << std::endl;

	GLenum err = glewInit(); // Initialise GLEW

	if (GLEW_OK != err)	// check if glewInit() failed
	{
		std::cerr << "OpenGL GLEW error: " << glewGetErrorString(err) << std::endl;
		throw exception_OpenGL_GLEW();
	}

	int gl_version[2] = { -1, -1 }; // check which OpenGL context version is in use
	glGetIntegerv(GL_MAJOR_VERSION, &gl_version[0]);
	glGetIntegerv(GL_MINOR_VERSION, &gl_version[1]);
	std::cout << "using OpenGL context version: " << gl_version[0] << "." << gl_version[1] << std::endl;

	std::cout << "OpenGL driver vendor: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "OpenGL driver renderer: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "OpenGL driver version: " << glGetString(GL_VERSION) << std::endl;
	//std::cout << "this OpenGL implementation supports the following extensions: " << std::endl << glGetString(GL_EXTENSIONS) << std::endl;
}

OpenGL3_Context::~OpenGL3_Context()
{
	std::cout << "an OpenGL context has been destroyed" << std::endl;
}

}
