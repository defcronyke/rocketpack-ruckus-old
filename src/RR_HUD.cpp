/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * RR_HUD.cpp
 *
 *  Created on: 2012-10-05
 *      Author: Jeremy Carter
 */

#include "../include/RR_HUD.hpp"

namespace def
{

RR_HUD::RR_HUD(def::OpenGL3_Context& opengl3_context)
	: _hud_shaders("shaders/hud.vert", "shaders/hud.frag"),
	  _hud_vao_buffer( new GLuint[1] ),
	  _hud_mvp_loc( new GLint[1] ),
	  _hud_tex_loc( new GLint[1] ),
	  _hud_rp_status_tex_buffer( new GLuint[1] ),
	  _hud_mvp( glm::ortho(0.0f, (GLfloat)opengl3_context.window_width, 0.0f, (GLfloat)opengl3_context.window_height, -1.0f, 1.0f) ),
	  _hud_rp_status_vertices_buffer( new GLuint[1] ),
	  _hud_rp_status_vertices( new GLfloat[12] ),
	  _hud_rp_status_colors_buffer( new GLuint[1] ),
	  _hud_rp_status_colors( new GLfloat[16] ),
	  _hud_rp_status_tex_coords_buffer( new GLuint[1] ),
	  _hud_rp_status_tex_coords( new GLfloat[8] ),
	  _hud_rp_status_normals_buffer( new GLuint[1] ),
	  _hud_rp_status_normals( new GLfloat[12] ),
	  _hud_first_render_error( true )
{
	hud_init(opengl3_context.window_width, opengl3_context.window_height);
}

RR_HUD::~RR_HUD()
{

}

void RR_HUD::hud_init(int resolution_width, int resolution_height)
{
	glGetError(); // clear error status so we can check if error occured inside
				  // this function
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	// set alpha blending mode

// begin rocketpack status icon initialization
	_hud_rp_status_image.loadFromFile("resources/hud/rocketpack_status_on.png");
	_hud_rp_status_image.flipVertically();

	float rp_status_size = 0.0009f;
	unsigned int rp_status_location_x = 0;
	unsigned int rp_status_location_y = _hud_rp_status_image.getSize().y * rp_status_size * resolution_height;
	unsigned int rp_status_width = _hud_rp_status_image.getSize().x * rp_status_size * resolution_width / 2;
	unsigned int rp_status_height = _hud_rp_status_image.getSize().y * rp_status_size * resolution_height;

	_hud_rp_status_vertices[0] = rp_status_location_x; _hud_rp_status_vertices[1] = rp_status_location_y; _hud_rp_status_vertices[2] = 0.0f;
	_hud_rp_status_vertices[3] = rp_status_location_x; _hud_rp_status_vertices[4] = rp_status_location_y - rp_status_height; _hud_rp_status_vertices[5] = 0.0f;
	_hud_rp_status_vertices[6] = rp_status_location_x + rp_status_width; _hud_rp_status_vertices[7] = rp_status_location_y - rp_status_height; _hud_rp_status_vertices[8] = 0.0f;
	_hud_rp_status_vertices[9] = rp_status_location_x + rp_status_width; _hud_rp_status_vertices[10] = rp_status_location_y; _hud_rp_status_vertices[11] = 0.0f;

	_hud_rp_status_colors[0] = 1.0f; _hud_rp_status_colors[1] = 1.0f; _hud_rp_status_colors[2] = 1.0f; _hud_rp_status_colors[3] = 0.0f;
	_hud_rp_status_colors[4] = 1.0f; _hud_rp_status_colors[5] = 1.0f; _hud_rp_status_colors[6] = 1.0f; _hud_rp_status_colors[7] = 0.0f;
	_hud_rp_status_colors[8] = 1.0f; _hud_rp_status_colors[9] = 1.0f; _hud_rp_status_colors[10] = 1.0f; _hud_rp_status_colors[11] = 0.0f;
	_hud_rp_status_colors[12] = 1.0f; _hud_rp_status_colors[13] = 1.0f; _hud_rp_status_colors[14] = 1.0f; _hud_rp_status_colors[15] = 0.0f;

	_hud_rp_status_tex_coords[0] = 0.0f; _hud_rp_status_tex_coords[1] = _hud_rp_status_image.getSize().y;
	_hud_rp_status_tex_coords[2] = 0.0f; _hud_rp_status_tex_coords[3] = 0.0f;
	_hud_rp_status_tex_coords[4] = _hud_rp_status_image.getSize().x; _hud_rp_status_tex_coords[5] = 0.0f;
	_hud_rp_status_tex_coords[6] = _hud_rp_status_image.getSize().x; _hud_rp_status_tex_coords[7] = _hud_rp_status_image.getSize().y;

	_hud_rp_status_normals[0] = 0.0f; _hud_rp_status_normals[1] = 0.0f; _hud_rp_status_normals[2] = 0.0f;
	_hud_rp_status_normals[3] = 0.0f; _hud_rp_status_normals[4] = 0.0f; _hud_rp_status_normals[5] = 0.0f;
	_hud_rp_status_normals[6] = 0.0f; _hud_rp_status_normals[7] = 0.0f; _hud_rp_status_normals[8] = 0.0f;
	_hud_rp_status_normals[9] = 0.0f; _hud_rp_status_normals[10] = 0.0f; _hud_rp_status_normals[11] = 0.0f;

	glGenVertexArrays(1, &_hud_vao_buffer[0]); // generate VAO
	glBindVertexArray(_hud_vao_buffer[0]);	   // bind VAO

	// vertices
	glEnableVertexAttribArray((GLuint)0);
	glGenBuffers(1, &_hud_rp_status_vertices_buffer[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _hud_rp_status_vertices_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), _hud_rp_status_vertices.get(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normals (inexplicably needed even though it won't be used in the shaders)
	glEnableVertexAttribArray((GLuint)1);
	glGenBuffers(1, &_hud_rp_status_normals_buffer[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _hud_rp_status_normals_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(GLfloat), _hud_rp_status_normals.get(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// texture
	glEnableVertexAttribArray((GLuint)2);
	glGenBuffers(1, &_hud_rp_status_tex_coords_buffer[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _hud_rp_status_tex_coords_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), _hud_rp_status_tex_coords.get(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenTextures(1, &_hud_rp_status_tex_buffer[0]);
	glBindTexture(GL_TEXTURE_RECTANGLE, _hud_rp_status_tex_buffer[0]);

	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA,
			_hud_rp_status_image.getSize().x, _hud_rp_status_image.getSize().y,
			0, GL_RGBA, GL_UNSIGNED_BYTE, _hud_rp_status_image.getPixelsPtr());
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	// colors
	glEnableVertexAttribArray((GLuint)3);
	glGenBuffers(1, &_hud_rp_status_colors_buffer[0]);
	glBindBuffer(GL_ARRAY_BUFFER, _hud_rp_status_colors_buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), _hud_rp_status_colors.get(), GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)3, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind buffers
	glBindVertexArray(0);

	_hud_mvp_loc[0] = glGetUniformLocation(_hud_shaders.id(), "mvpMatrix");
	_hud_tex_loc[0] = glGetUniformLocation(_hud_shaders.id(), "rpStatusTex");
// end rocketpack status icon initialization

	// check for OpenGL errors
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
		std::cerr << "GL Error in Lua_Level_Loader::hud_init(): "
				  << std::hex << error << std::endl;

	return;
}

void RR_HUD::hud_display_rp_on_icon()
{
	glGetError(); // clear error status so we can check if error occured inside
				  // this function
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	_hud_shaders.bind();

		glUniformMatrix4fv(_hud_mvp_loc[0], 1, GL_FALSE, &_hud_mvp[0][0]);

		//------begin rocketpack status icon rendering-------
		glUniform1i(_hud_tex_loc[0], 0);

		glBindTexture(GL_TEXTURE_RECTANGLE, _hud_rp_status_tex_buffer[0]);

//		// pinpoint OpenGL errors
//		GLenum error = glGetError();
//		if (error != GL_NO_ERROR) std::cerr << "GL Error: " << std::hex << error << std::endl;
//		else std::cout << "GL Error: no error" << std::endl;

		glBindVertexArray(_hud_vao_buffer[0]);	   // bind the VAO

		// draw the contents of the bound VAO
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

		glBindVertexArray(0); // unbind the VAO

		glBindTexture(GL_TEXTURE_RECTANGLE, 0);
		//------end rocketpack status icon rendering-------

	_hud_shaders.unbind();

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	// check for OpenGL errors
	GLenum error = glGetError();
	if (error != GL_NO_ERROR && _hud_first_render_error)
	{
		std::cerr << "GL Error in Lua_Level_Loader::hud_display_rp_on_icon(): "
				  << std::hex << error << std::endl;
		_hud_first_render_error = false;
	}

	return;
}

}
