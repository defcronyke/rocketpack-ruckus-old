/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Shader.cpp
 *
 *  Created on: 2012-06-19
 *      Author: Jeremy Carter
 */

#include "../include/Shader.hpp"

namespace def
{

Shader::Shader(const std::string& vertex_shader_filename,	///<[in] path to vertex shader file
			   const std::string& fragment_shader_filename) ///<[in] path to fragment shader file
	: _shader_id(0),
	  _shader_vp(0),
	  _shader_fp(0),
	  _vertex_shader_file(vertex_shader_filename),
	  _fragment_shader_file(fragment_shader_filename)
{
	init(vertex_shader_filename, fragment_shader_filename);
}

Shader::Shader()
	: _shader_id(0),
	  _shader_vp(0),
	  _shader_fp(0)
{

}

Shader::~Shader()
{
	if (_shader_id)
	{
		glDetachShader(_shader_id, _shader_fp);
		glDetachShader(_shader_id, _shader_vp);

		glDeleteShader(_shader_fp);
		glDeleteShader(_shader_vp);
		glDeleteProgram(_shader_id);
	}
}

void Shader::init(const std::string& vertex_shader_filename,	///<[in] path to vertex shader file
				  const std::string& fragment_shader_filename)	///<[in] path to fragment shader file
{
	std::cout << "loading vertex shader: " << vertex_shader_filename << std::endl;
	std::cout << "loading fragment shader: " << fragment_shader_filename << std::endl;

	// create shaders and grab a handle to them
	_shader_vp = glCreateShader(GL_VERTEX_SHADER);
	_shader_fp = glCreateShader(GL_FRAGMENT_SHADER);

	// grab contents of each shader file
	const char* vertex_shader_file_content = _vertex_shader_file.content().c_str();
	const char* fragment_shader_file_content = _fragment_shader_file.content().c_str();

	// provide shader source to OpenGL
	glShaderSource(_shader_vp, 1, &vertex_shader_file_content, 0);
	glShaderSource(_shader_fp, 1, &fragment_shader_file_content, 0);

	// compile the shaders
	glCompileShader(_shader_vp);
	GLint vertex_compile_status = GL_FALSE;
	glGetShaderiv(_shader_vp, GL_COMPILE_STATUS, &vertex_compile_status);
	if (vertex_compile_status == GL_TRUE)
		std::cout << "vertex shader " << vertex_shader_filename << " compile status: success" << std::endl;
	else
		std::cerr << "vertex shader " << vertex_shader_filename << " compile status: fail\n" << std::endl;

	glCompileShader(_shader_fp);
	GLint fragment_compile_status = GL_FALSE;
	glGetShaderiv(_shader_fp, GL_COMPILE_STATUS, &fragment_compile_status);
	if (fragment_compile_status == GL_TRUE)
		std::cout << "fragment shader " << fragment_shader_filename << " compile status: success" << std::endl;
	else
		std::cerr << "fragment shader " << fragment_shader_filename << " compile status: fail\n" << std::endl;

	// create the shader program and get a handle to it
	_shader_id = glCreateProgram();

	// attach the shaders to the new shader program
	glAttachShader(_shader_id, _shader_vp);
	glAttachShader(_shader_id, _shader_fp);

	// link the program
	glLinkProgram(_shader_id);
	GLint link_status;
	glGetProgramiv(_shader_id, GL_LINK_STATUS, &link_status);

	if (link_status == GL_TRUE)
		std::cout << "shader program link status: success" << std::endl;
	else
		std::cerr << "shader program link status: fail" << std::endl;

	if (vertex_compile_status == GL_FALSE ||
		fragment_compile_status == GL_FALSE ||
		link_status == GL_FALSE)
	{
		GLint shader_log_length = 0;
		glGetShaderiv(_shader_id, GL_INFO_LOG_LENGTH, &shader_log_length);
		if (shader_log_length > 0)
		{
			GLchar shader_info_log[shader_log_length];
			for (int i = 0; i < shader_log_length; i++)
				shader_info_log[i] = (GLchar)NULL;

			glGetShaderInfoLog(_shader_id, shader_log_length, NULL, shader_info_log);
			std::cerr << "shader info log: " << std::endl << std::endl
					  << shader_info_log << std::endl;
		}

		GLint program_log_length = 0;
		glGetProgramiv(_shader_id, GL_INFO_LOG_LENGTH, &program_log_length);
		if (program_log_length > 0)
		{
			GLchar program_info_log[program_log_length];
			for (int i = 0; i < program_log_length; i++)
				program_info_log[i] = (GLchar)NULL;

			glGetProgramInfoLog(_shader_id, program_log_length, NULL, program_info_log);
			std::cerr << "shader program info log: " << std::endl << std::endl
					  << program_info_log << std::endl;
		}
	}

	// supply vertices and normals to the vertex shader
	glBindAttribLocation(_shader_id, 0, "in_Position");
	glBindAttribLocation(_shader_id, 1, "in_Normal");
	glBindAttribLocation(_shader_id, 2, "in_UV");
	glBindAttribLocation(_shader_id, 3, "in_Color");
}

unsigned int Shader::id()
{
	return _shader_id;	/// @return the shader id handle
}

void Shader::bind()
{
	glUseProgram(_shader_id);
}

void Shader::unbind()
{
	glUseProgram(0);
}

}
