// get config from autotools
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "../../include/File.hpp"
#include "../../include/assert.hpp"
#include <iostream>

/// Test Suite: File Test 1 - Open a file and correctly read its contents

int main(int argc, char** argv)
{
	std::cout << "File_test1: checking file read ability..." << std::endl;

	def::File test_file(SRCDIR"/src/test_suite/File_test1-test_file");

	ASSERT(test_file.content() == "blahblahbloobleebloo\n",
		   "Error: failed to read content of test file (or test file corrupted). "
		   "The file content we're getting: " << test_file.content());

	std::cout << "File_test1: read OK" << std::endl;
	return 0;
}
