// get config from autotools
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "../../include/Controls.hpp"
#include <iostream>
#include <string>
#include <map>
#include <vector>


int main(int argc, char** argv)
{
	std::cout << "Controls_test1: checking if controls.conf has "
			     "all the required sections..." << std::endl;

	def::Controls controls(SRCDIR"/conf/controls.conf");

	std::vector< std::string > necessary_sections;
	std::vector< std::string > missing_sections;

	necessary_sections.push_back("Left");
	necessary_sections.push_back("Right");
	necessary_sections.push_back("Up");
	necessary_sections.push_back("Down");
	necessary_sections.push_back("Jump");
	necessary_sections.push_back("Pause");
	necessary_sections.push_back("Look");

	std::map< std::string, std::string >::iterator it;
	for (it = controls.controls_conf_map.begin(); it != controls.controls_conf_map.end(); ++it)
	{
		std::cout << (*it).first << " : " << (*it).second << std::endl;
	}

	std::vector< std::string >::iterator it2;
	for (it2 = necessary_sections.begin(); it2 != necessary_sections.end(); ++it2)
	{
		if (controls.controls_conf_map.find((*it2)) == controls.controls_conf_map.end())
			missing_sections.push_back((*it2));
	}

	if (missing_sections.size() > 0)
	{
		std::cout << "Error: missing sections: " << std::endl;
		std::vector< std::string >::iterator it3;
		for (it3 = missing_sections.begin(); it3 != missing_sections.end(); ++it3)
		{
			std::cout << (*it3) << " ";
		}
		std::cout << std::endl;

		return 1;
	}

	std::cout << "Controls_test1: all required sections are present" << std::endl;

	return 0;
}
