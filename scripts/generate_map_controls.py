#!/usr/bin/python
#
# *	Copyright © 2012 Jeremy Carter
# * --------------------------------
# *  This file is part of Rocketpack Ruckus.
# *
# *  Rocketpack Ruckus is free software: you can redistribute it and/or
# *  modify it under the terms of the GNU General Public License version 3,
# *  as published by the Free Software Foundation.
# *
# *  Rocketpack Ruckus is distributed in the hope that it will be useful,
# *  but WITHOUT ANY WARRANTY; without even the implied warranty of
# *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *  GNU General Public License for more details.
# *
# *  You should have received a copy of the GNU General Public License
# *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
# * --------------------------------
#
# outputs the code that goes in Controls::_map_controls()
#
# add support for mapping more controls in here if you want,
# then simply run this script and the necessary code for the
# _map_controls() function will be printed to stdout
#
# NOTE: SFML Tilde support is currently broken on Linux
#		( see: https://github.com/LaurentGomila/SFML/issues/222 )

actions = ["Left", "Right", "Up", "Down", "Jump", "Rocketpack", "Pause"]

for i in range(len(actions)):
	print """	if (controls_conf_map.find(\""""+actions[i]+"""\") != controls_conf_map.end())
	{"""

	for j in range(ord('A'), ord('Z')+1): # map keyboard letters A-Z
		print """		if (controls_conf_map[\""""+actions[i]+"""\"] == \""""+chr(j)+"""\")
			"""+actions[i]+""" = sf::Keyboard::"""+chr(j)+";"
	
	for j in range(ord('0'), ord('9')+1): # map keyboard numbers and numpad numbers 0-9
		print """		if (controls_conf_map[\""""+actions[i]+"""\"] == \"Num"""+chr(j)+"""\")
			"""+actions[i]+""" = sf::Keyboard::Num"""+chr(j)+";"
		print """		if (controls_conf_map[\""""+actions[i]+"""\"] == \"Numpad"""+chr(j)+"""\")
			"""+actions[i]+""" = sf::Keyboard::Numpad"""+chr(j)+";"
	
	for j in range(1, 13): # map F1-F12
		print """		if (controls_conf_map[\""""+actions[i]+"""\"] == \"F"""+str(j)+"""\")
			"""+actions[i]+""" = sf::Keyboard::F"""+str(j)+";"
	
	# individual mappings
	print """		if (controls_conf_map[\""""+actions[i]+"""\"] == "Space")
			"""+actions[i]+""" = sf::Keyboard::Space;		
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Left")
			"""+actions[i]+""" = sf::Keyboard::Left;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Right")
			"""+actions[i]+""" = sf::Keyboard::Right;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Up")
			"""+actions[i]+""" = sf::Keyboard::Up;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Down")
			"""+actions[i]+""" = sf::Keyboard::Down;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Dash")
			"""+actions[i]+""" = sf::Keyboard::Dash;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Equal")
			"""+actions[i]+""" = sf::Keyboard::Equal;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "BackSpace")
			#ifdef COMPILING_FOR_WINDOWS
			"""+actions[i]+""" = sf::Keyboard::Back;
			#else
			"""+actions[i]+""" = sf::Keyboard::BackSpace;
			#endif
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Tab")
			"""+actions[i]+""" = sf::Keyboard::Tab;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "SemiColon")
			"""+actions[i]+""" = sf::Keyboard::SemiColon;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Quote")
			"""+actions[i]+""" = sf::Keyboard::Quote;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Comma")
			"""+actions[i]+""" = sf::Keyboard::Comma;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Period")
			"""+actions[i]+""" = sf::Keyboard::Period;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Slash")
			"""+actions[i]+""" = sf::Keyboard::Slash;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "LBracket")
			"""+actions[i]+""" = sf::Keyboard::LBracket;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "RBracket")
			"""+actions[i]+""" = sf::Keyboard::RBracket;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "BackSlash")
			"""+actions[i]+""" = sf::Keyboard::BackSlash;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Return")
			"""+actions[i]+""" = sf::Keyboard::Return;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "LShift")
			"""+actions[i]+""" = sf::Keyboard::LShift;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "RShift")
			"""+actions[i]+""" = sf::Keyboard::RShift;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "LControl")
			"""+actions[i]+""" = sf::Keyboard::LControl;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "RControl")
			"""+actions[i]+""" = sf::Keyboard::RControl;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "LAlt")
			"""+actions[i]+""" = sf::Keyboard::LAlt;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "RAlt")
			"""+actions[i]+""" = sf::Keyboard::RAlt;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Home")
			"""+actions[i]+""" = sf::Keyboard::Home;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "End")
			"""+actions[i]+""" = sf::Keyboard::End;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Delete")
			"""+actions[i]+""" = sf::Keyboard::Delete;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "PageUp")
			"""+actions[i]+""" = sf::Keyboard::PageUp;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "PageDown")
			"""+actions[i]+""" = sf::Keyboard::PageDown;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Divide")
			"""+actions[i]+""" = sf::Keyboard::Divide;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Multiply")
			"""+actions[i]+""" = sf::Keyboard::Multiply;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Subtract")
			"""+actions[i]+""" = sf::Keyboard::Subtract;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Add")
			"""+actions[i]+""" = sf::Keyboard::Add;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "LSystem")
			"""+actions[i]+""" = sf::Keyboard::LSystem;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "RSystem")
			"""+actions[i]+""" = sf::Keyboard::RSystem;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Menu")
			"""+actions[i]+""" = sf::Keyboard::Menu;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Insert")
			"""+actions[i]+""" = sf::Keyboard::Insert;
		if (controls_conf_map[\""""+actions[i]+"""\"] == "Pause")
			"""+actions[i]+""" = sf::Keyboard::Pause;
	}
	"""