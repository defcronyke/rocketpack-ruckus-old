#version 150 core

in vec3 normal;
in vec3 lightDir;
in vec3 eyeVec;
in vec2 UV;

uniform vec4 globalAmbient;
uniform vec4 materialAmbient;
uniform vec4 materialDiffuse;
uniform vec4 materialSpecular;
uniform vec4 lightAmbient;
uniform vec4 lightDiffuse;
uniform vec4 lightSpecular;
uniform sampler2D textureSampler;

out vec4 out_Color;

void main()
{
	vec4 matAmb = vec4(max(materialAmbient.r, 0.1), max(materialAmbient.g, 0.1), max(materialAmbient.b, 0.1), materialAmbient.a);
	vec4 matDiff = vec4(max(materialDiffuse.r, 0.5), max(materialDiffuse.g, 0.5), max(materialDiffuse.b, 0.5), materialDiffuse.a);
	vec4 matSpec = vec4(max(materialSpecular.r, 0.5), max(materialSpecular.g, 0.5), max(materialSpecular.b, 0.5), materialSpecular.a);

	vec4 final_color = (globalAmbient * matAmb) +
					   (lightAmbient * matAmb);
					   
	vec3 N = normalize(normal);
	vec3 L = normalize(lightDir);
	
	float lambertTerm = dot(N, L);
	
	if (lambertTerm > 0.0)
	{
		final_color += lightDiffuse * matDiff * lambertTerm;
		
		vec3 E = normalize(eyeVec);
		vec3 R = reflect(-L, N);
		float specular = pow(max(dot(R, E), 0.0), 255.0);
		final_color += lightSpecular * matSpec * specular;
	}
	
	final_color.rgb += texture2D(textureSampler, UV).rgb;
	
	out_Color = final_color;
}