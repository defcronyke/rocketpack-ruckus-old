#version 150 core

in vec4 in_Position;
in vec3 in_Normal;
in vec2 in_UV;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 modelviewMatrix;
uniform mat4 modelviewprojectionMatrix;
uniform mat3 normalMatrix;
uniform vec4 lightPosition;

out vec3 normal;
out vec3 lightDir;
out vec3 eyeVec;
out vec2 UV;

void main()
{		
	normal = normalMatrix * in_Normal;
	vec3 vVertex = vec3(modelviewMatrix * in_Position);
	
	lightDir = vec3(lightPosition.xyz - vVertex);
		
	eyeVec = -vVertex;
	
	gl_Position = modelviewprojectionMatrix * in_Position;
	UV = in_UV;
}