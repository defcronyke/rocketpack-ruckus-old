# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/src/levels/test_00/Test_World_Pause_Menu.cpp" "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defTest_World_Pause_Menu.dir/levels/test_00/Test_World_Pause_Menu.cpp.obj"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "COMPILING_FOR_WINDOWS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defFile.dir/DependInfo.cmake"
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defShader.dir/DependInfo.cmake"
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defAssimp_Resource.dir/DependInfo.cmake"
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defControls.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../../include"
  "."
  "../../include/external/windows"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
