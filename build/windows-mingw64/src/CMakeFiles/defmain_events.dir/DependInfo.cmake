# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/src/main_events.cpp" "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defmain_events.dir/main_events.cpp.obj"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "COMPILING_FOR_WINDOWS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defOpenGL3_Context.dir/DependInfo.cmake"
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defControls.dir/DependInfo.cmake"
  "C:/Users/bakeonandham/Documents/eclipse_workspace/rocketpack-ruckus/build/windows-mingw64/src/CMakeFiles/defFile.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../../include"
  "."
  "../../include/external/windows"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
