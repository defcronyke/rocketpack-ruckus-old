// This file is used to pass info from CMake
// to the source code for the project.

#define rocketpack_ruckus_VERSION_MAJOR 0
#define rocketpack_ruckus_VERSION_MINOR 0

#define DATADIR "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus"
#define PROJECTNAME "rocketpack-ruckus"
