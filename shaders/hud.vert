// *  Copyright © 2012 Jeremy Carter
// * --------------------------------
// *  This file is part of Rocketpack Ruckus.
// *
// *  Rocketpack Ruckus is free software: you can redistribute it and/or
// *  modify it under the terms of the GNU General Public License version 3,
// *  as published by the Free Software Foundation.
// *
// *  Rocketpack Ruckus is distributed in the hope that it will be useful,
// *  but WITHOUT ANY WARRANTY; without even the implied warranty of
// *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *  GNU General Public License for more details.
// *
// *  You should have received a copy of the GNU General Public License
// *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
// * --------------------------------

#version 150

in vec4 in_Position;
in vec3 in_Normal;
in vec2 in_UV;
in vec4 in_Color;

uniform mat4 mvpMatrix;

smooth out vec3 vVaryingNormal;
smooth out vec2 vVaryingTexCoord;
smooth out vec4 vVaryingColor;

void main(void) 
{
	vVaryingNormal = in_Normal;
	vVaryingTexCoord = in_UV;
	vVaryingColor = in_Color;
    
    gl_Position = mvpMatrix * in_Position;
}
