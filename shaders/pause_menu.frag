// *  Copyright © 2012 Jeremy Carter
// * --------------------------------
// *  This file is part of Rocketpack Ruckus.
// *
// *  Rocketpack Ruckus is free software: you can redistribute it and/or
// *  modify it under the terms of the GNU General Public License version 3,
// *  as published by the Free Software Foundation.
// *
// *  Rocketpack Ruckus is distributed in the hope that it will be useful,
// *  but WITHOUT ANY WARRANTY; without even the implied warranty of
// *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *  GNU General Public License for more details.
// *
// *  You should have received a copy of the GNU General Public License
// *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
// * --------------------------------

#version 150

smooth in vec3 vVaryingNormal;
smooth in vec2 vVaryingTexCoord;
smooth in vec4 vVaryingColor;

uniform sampler2DRect pauseMenuTex;

out vec4 out_Color;

void main(void)
{ 
    out_Color.a = vVaryingColor.a;
    out_Color += texture(pauseMenuTex, vVaryingTexCoord);
}
