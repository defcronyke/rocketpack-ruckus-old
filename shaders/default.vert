// *  Copyright © 2012 Jeremy Carter
// * --------------------------------
// *  This file is part of Rocketpack Ruckus.
// *
// *  Rocketpack Ruckus is free software: you can redistribute it and/or
// *  modify it under the terms of the GNU General Public License version 3,
// *  as published by the Free Software Foundation.
// *
// *  Rocketpack Ruckus is distributed in the hope that it will be useful,
// *  but WITHOUT ANY WARRANTY; without even the implied warranty of
// *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *  GNU General Public License for more details.
// *
// *  You should have received a copy of the GNU General Public License
// *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
// * --------------------------------

#version 150 core

in vec4 in_Position;
in vec3 in_Normal;
in vec2 in_UV;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 modelviewMatrix;
uniform mat4 modelviewprojectionMatrix;
uniform mat3 normalMatrix;
uniform vec4 lightPosition;
uniform mat4 translationMatrix;
uniform mat4 rotationMatrix;

out vec3 normal;
out vec3 lightDir;
out vec3 eyeVec;
smooth out vec2 UV;

void main()
{		
	normal = normalMatrix * in_Normal;
	//normal.x = -normal.x;
	//normal.z = -normal.z;
	
	vec4 position = in_Position;
	//position.x = -position.x;
	//position.z = -position.z;
	
	vec4 light_position = lightPosition;
	//light_position.x = -light_position.x;
	//light_position.z = -light_position.z;
	
	vec3 vVertex = vec3(modelviewMatrix * position);
	
	lightDir = vec3(light_position.xyz - vVertex);
		
	eyeVec = -vVertex;
	
	UV = in_UV;
	gl_Position = modelviewprojectionMatrix * translationMatrix * rotationMatrix * position;
}
