/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * main_loop.hpp
 *
 *  Created on: 2012-07-10
 *      Author: Jeremy Carter
 */

#ifndef MAIN_LOOP_HPP_
#define MAIN_LOOP_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include "OpenGL3_Context.hpp"
#include "main_events.hpp"
#include "Lua_Level_Loader.hpp"
#include "Shader.hpp"

namespace def
{

/// this is where the action is
void main_loop( def::OpenGL3_Context& opengl3_context );

}

#endif /* MAIN_LOOP_HPP_ */
