/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * RR_Object.hpp
 *
 *  Created on: 2012-09-29
 *      Author: Jeremy Carter
 */

#ifndef RR_OBJECT_HPP_
#define RR_OBJECT_HPP_

#include <iostream>
#include <string>
#include "Assimp_Resource.hpp"
#include "Shader.hpp"

namespace def
{

class RR_Object: public def::Assimp_Resource
{
public:
	std::string vert_shader_filename;
	std::string frag_shader_filename;
	def::Shader shaders;

	RR_Object(const std::string& model_filename, const std::string& texture_filename, glm::mat4& modelviewprojection_matrix);
	RR_Object();
	virtual ~RR_Object();

	bool uses_default_vert_shader(); ///< Check if this object uses the default vertex shader.
	bool uses_default_frag_shader(); ///< Check if this object uses the default fragment shader.
	void set_vert_shader(const std::string& vertex_shader);	///< Override the default vertex shader and use a custom one instead.
	void set_frag_shader(const std::string& fragment_shader); ///< Override the default fragment shader and use a custom one instead.
	void init_shaders(); ///< Load the shaders.
};

}

#endif /* RR_OBJECT_HPP_ */
