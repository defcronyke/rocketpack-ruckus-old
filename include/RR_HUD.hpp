/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * RR_HUD.hpp
 *
 *  Created on: 2012-10-05
 *      Author: Jeremy Carter
 */

#ifndef RR_HUD_HPP_
#define RR_HUD_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <iostream>
#include <string>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include "OpenGL3_Context.hpp"
#include "Shader.hpp"

namespace def
{

class RR_HUD
{
	def::Shader _hud_shaders;
	sf::Image _hud_rp_status_image;
	std::unique_ptr< GLuint[] > _hud_vao_buffer;
	std::unique_ptr< GLint[] > _hud_mvp_loc;
	std::unique_ptr< GLint[] > _hud_tex_loc;
	std::unique_ptr< GLuint[] > _hud_rp_status_tex_buffer;
	glm::mat4 _hud_mvp;
	std::unique_ptr< GLuint[] > _hud_rp_status_vertices_buffer;
	std::unique_ptr< GLfloat[] > _hud_rp_status_vertices;
	std::unique_ptr< GLuint[] > _hud_rp_status_colors_buffer;
	std::unique_ptr< GLfloat[] > _hud_rp_status_colors;
	std::unique_ptr< GLuint[] > _hud_rp_status_tex_coords_buffer;
	std::unique_ptr< GLfloat[] > _hud_rp_status_tex_coords;
	std::unique_ptr< GLuint[] > _hud_rp_status_normals_buffer;
	std::unique_ptr< GLfloat[] > _hud_rp_status_normals;
	bool _hud_first_render_error;

public:
	RR_HUD(def::OpenGL3_Context& opengl3_context);
	virtual ~RR_HUD();

	void hud_init(int resolution_width, int resolution_height);
	void hud_display_rp_on_icon();	///< Display an icon when the rocketpack is on.
};

}

#endif /* RR_HUD_HPP_ */
