/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * File.hpp
 *
 *  Created on: 2012-06-19
 *      Author: Jeremy Carter
 */

#ifndef FILE_HPP_
#define FILE_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <iostream>
#include <string>
#include <fstream>
#include <memory>
#include <stdexcept>

namespace def
{

/// an exception for when unable to open file
class exception_File : public std::runtime_error
{
public:
	exception_File(): runtime_error("unable to open file"){};
};

class File
{
	std::string _filename;
	std::ifstream _filestream;
	std::string _content;

public:
	File(const std::string& filename);
	File();
	~File();

	void open(const std::string& filename);
	const std::string& content();
};

}

#endif /* FILE_HPP_ */
