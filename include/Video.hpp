/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Video.hpp
 *
 *  Created on: 2012-08-27
 *      Author: Jeremy Carter
 */

#ifndef VIDEO_HPP_
#define VIDEO_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <boost/algorithm/string.hpp>
#include "File.hpp"

namespace def
{

/// Parses video.conf
class Video
{
	def::File _video_conf;	///< The video.conf file

	void _parse_resolution();

public:
	std::map< std::string, std::string > video_conf_map;
	int width, height;
	bool fullscreen;

	Video(const std::string& video_conf_file);
	virtual ~Video();
};

}

#endif /* VIDEO_HPP_ */
