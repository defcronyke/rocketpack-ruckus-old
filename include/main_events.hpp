/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * events_main.hpp
 *
 *  Created on: 2012-06-19
 *      Author: Jeremy Carter
 */

#ifndef EVENTS_MAIN_HPP_
#define EVENTS_MAIN_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <GL/glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include "OpenGL3_Context.hpp"

namespace def
{

void handle_main_events(OpenGL3_Context& opengl3_context, sf::Event& event);

}

#endif /* EVENTS_MAIN_HPP_ */
