/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 * RR_Player.hpp
 *
 *  Created on: 2012-10-04
 *      Author: Jeremy Carter
 */

#ifndef RR_PLAYER_HPP_
#define RR_PLAYER_HPP_

#include "Assimp_Resource.hpp"

namespace def
{

class RR_Player: public def::Assimp_Resource
{
public:
	RR_Player();
	virtual ~RR_Player();
};

}

#endif /* RR_PLAYER_HPP_ */
