/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Controls.hpp
 *
 *  Created on: 2012-07-13
 *      Author: Jeremy Carter
 */

#ifndef CONTROLS_HPP_
#define CONTROLS_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <boost/algorithm/string.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include "File.hpp"

namespace def
{

/// Parses controls.conf
class Controls
{
	def::File _controls_conf;	///< The controls.conf file object.
	void _map_controls();		///< Does the control mapping.

public:
	std::map< std::string, std::string > controls_conf_map;	///< A map that holds the controls.conf pairings.
	sf::Keyboard::Key Left, Right, Up, Down, Jump, Rocketpack, Pause;	///< The mapped actions. Use these in the game.

	Controls(const std::string& controls_conf_file);	///< Parses the controls.conf file.
	virtual ~Controls();
};

}

#endif /* CONTROLS_HPP_ */
