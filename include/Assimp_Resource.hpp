/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Assimp_Resource.hpp
 *
 *  Created on: 2012-06-21
 *      Author: Jeremy Carter
 */

#ifndef ASSIMP_HPP_
#define ASSIMP_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <iostream>
#include <memory>
#include <string>
#include <cstring>
#include <stdexcept>
#include <fstream>
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics/Image.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <assimp/assimp.hpp>		///< importer interface
#include <assimp/aiScene.h>			///< output data structure
#include <assimp/aiPostProcess.h>	///< post processing flags

#include "Shader.hpp"

namespace def
{

/// an exception for when unable to open a model file with Assimp
class exception_Assimp : public std::runtime_error
{
public:
	exception_Assimp(): runtime_error("unable to open file"){};
};

/// an exception for when unable to open a texture image file with SFML
class exception_SFML : public std::runtime_error
{
public:
	exception_SFML(): runtime_error("unable to open file"){};
};

/// the asset import library helper class
/** This can be used to load models which have
	been exported from a 3D modeling program. */
class Assimp_Resource
{
protected:
	const aiScene* _scene;				///< everything in the exported model
	Assimp::Importer _importer;			///< the assimp import helper

	glm::mat4 _modelviewprojection_matrix;
	glm::mat4 _translation_matrix;	///< for positioning the objects
	glm::mat4 _rotation_matrix; ///< for rotating the objects
	glm::vec4 _position;
	double _rotation;

	glm::vec4 _material_ambient;
	glm::vec4 _material_diffuse;
	glm::vec4 _material_specular;

public:
	std::unique_ptr< GLuint[] > vao_id;	///< an array of all VAO handles
	std::unique_ptr< GLuint[] > vbo_id;	///< an array of all VBO handles
	std::unique_ptr< GLuint[] > ibo_id;	///< an array of all IBO handles
	std::unique_ptr< GLuint[] > tex_id;	///< an array of all texture handles
	std::unique_ptr< GLuint[] > uv_buffer; ///< the uv buffer
	GLuint num_meshes;

	/// load a model from a file
	Assimp_Resource(const std::string& model_filename, const std::string& texture_filename, glm::mat4& modelviewprojection_matrix);
	Assimp_Resource();
	virtual ~Assimp_Resource();

	void load(const std::string& model_filename, const std::string& texture_filename);
	void display(def::Shader& shaders);	///< display the model

	void set_pos(double x, double y, double z);
	void set_x_pos(double x);
	void set_y_pos(double y);
	void set_z_pos(double z);
	void set_rot(double degrees);
	double get_x_pos();
	double get_y_pos();
	double get_z_pos();
	double get_rot();
};

}

#endif /* ASSIMP_HPP_ */
