/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Lua_Level_Loader.hpp
 *
 *  Created on: 2012-09-17
 *      Author: Jeremy Carter
 */

#ifndef LUA_LEVEL_LOADER_HPP_
#define LUA_LEVEL_LOADER_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <deque>
#include <algorithm>
#include <map>
#include <vector>
#include "lua.hpp"
#include "File.hpp"
#include "OpenGL3_Context.hpp"
#include "Assimp_Resource.hpp"
#include "RR_Object.hpp"
#include "Shader.hpp"
#include "Controls.hpp"
#include "RR_Pause_Menu.hpp"
#include "RR_HUD.hpp"

#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#include <math.h>

namespace def
{

class Lua_Level_Loader
{
	def::OpenGL3_Context& _opengl3_context;

	lua_State* _L; ///< A full Lua environment. Only use this with trusted scripts!
	lua_State* _sandboxed_L; ///< A stripped-down Lua environment for running untrusted scripts.
	unsigned int _levelnumber;
	std::string _levelpack;
	def::File _Lua_level_script;
	size_t _num_objects;
	std::string _levelstring;

	#ifndef COMPILING_FOR_WINDOWS	// if compiling for Linux
		struct stat _level_script_attributes;
		struct stat _level_script_old_attributes;
	#else
		struct _stat64i32 _level_script_attributes;
		struct _stat64i32 _level_script_old_attributes;
	#endif

	float _resolution_width;
	float _resolution_height;

	def::Controls _controls; ///< holds control mappings it gets from controls.conf

	float _main_character_height;	///< /todo move this into main character class

	float _initial_fov;
	float _fov;

	def::Shader _shaders;			///< the shader handling object

	GLint _projection_matrix_location;
	GLint _view_matrix_location;
	GLint _model_matrix_location;
	GLint _modelview_matrix_location;
	GLint _modelviewprojection_matrix_location;
	GLint _normal_matrix_location;
	GLint _global_ambient_location;
	GLint _light_ambient_location;
	GLint _light_diffuse_location;
	GLint _light_specular_location;
	GLint _light_position_location;
	GLint _texture_sampler_location;

	glm::mat4 _projection_matrix;	///< the projection matrix
	glm::mat4 _view_matrix;			///< the view matrix
	glm::mat4 _model_matrix;		///< the model matrix
	glm::mat4 _modelview_matrix;
	glm::mat4 _modelviewprojection_matrix;
	glm::mat4 _normal_matrix;		///< the normal matrix

	glm::vec4 _global_ambient;

	glm::vec4 _light_ambient;		///< the ambient RGB colour component
	glm::vec4 _light_diffuse;		///< the diffuse RGB colour component
	glm::vec4 _light_specular;		///< the specular RGB colour component
	glm::vec4 _light_position;		///< the position of the light source

	bool _paused;

	int _mouse_x_pos;
	int _mouse_y_pos;
	int _mouse_last_x_pos;
	int _mouse_last_y_pos;

	glm::vec3 _position;
	glm::vec3 _direction;
	glm::vec3 _right;
	glm::vec3 _up;
	float _horizontal_angle;
	float _vertical_angle;
	float _speed;
	float _mouse_speed;
	sf::Window* _window;
	sf::Vector2i _middle_of_window;
	bool _jumping;
	bool _stopped_jumping;
	bool _rocketpack_is_on;
	float _jump_speed;
	float _jump_height;
	float _jump_hang_time;
	float _rocketpack_speed;
	float _rocketpack_up_speed;
	unsigned int _rocketpack_toggle_counter;
	bool _standing; ///< standing on something
	float _distance_from_ground;
	float _global_gravity;

	std::string _get_Lua_sandboxing_script();
	void _print_Lua_script_error(lua_State* L, std::string& levelstring);

public:
	std::unique_ptr< def::RR_Object[] > objects; ///< All objects that are in the level.
	def::RR_Pause_Menu pause_menu;
	def::RR_HUD hud;

	Lua_Level_Loader(
		def::OpenGL3_Context& opengl3_context,
		unsigned int levelnumber = 0, ///< Which level to load from a level pack.
	    std::string levelpack = "official" /**< To load 3rd-party level packs,
	    										call this with "unofficial/packname". */
	);

	virtual ~Lua_Level_Loader();

	unsigned int get_levelnumber();

	bool load_level( def::OpenGL3_Context& opengl3_context,
					 unsigned int levelnumber = 0,
					 std::string levelpack = "official" );

	double diffpos(glm::vec3& a, glm::vec3& b); ///< Calculate the distance between two positions.

	void sort_objects( std::unique_ptr< def::RR_Object[] >& objects_to_sort, std::vector< std::pair< unsigned int, double > >& distances_from_camera );

	void display();

	void handle_events(float& time);	///< world-specific event handler
};

}

#endif /* LUA_LEVEL_LOADER_HPP_ */
