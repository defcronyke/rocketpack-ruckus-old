/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * OpenGL3_Context.hpp
 *
 *  Created on: 2012-06-19
 *      Author: Jeremy Carter
 */

#ifndef OPENGL3_CONTEXT_HPP_
#define OPENGL3_CONTEXT_HPP_

// get config from autotools
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <memory>
#include <string>
#include <stdexcept>

#include <GL/glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace def
{

/// an exception for when glewInit() fails
class exception_OpenGL_GLEW : public std::runtime_error
{
public:
	exception_OpenGL_GLEW(): runtime_error("GLEW initialization failed"){};
};

class OpenGL3_Context
{
	std::string _window_title;
	int _window_bpp;
	sf::ContextSettings _context_settings;
	glm::mat4 _projection_matrix;
	glm::mat4 _view_matrix;
	glm::mat4 _model_matrix;

public:
	int window_width;
	int window_height;
	std::unique_ptr< sf::Window > window;

	OpenGL3_Context( const int width = 800,
					 const int height = 600,
					 const bool fullscreen = false,
					 std::string title = "A Cross-Platform OpenGL 3.x Program",
					 const int antialiasing_level = 0,
					 const int major_version = 3,
					 const int minor_version = 3,
					 const int bpp = 32,
					 const int stencil = 0 );
	virtual ~OpenGL3_Context();
};

}

#endif /* OPENGL3_CONTEXT_HPP_ */
