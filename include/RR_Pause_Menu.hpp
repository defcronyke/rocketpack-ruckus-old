/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * RR_Pause_Menu.hpp
 *
 *  Created on: 2012-10-05
 *      Author: Jeremy Carter
 */

#ifndef RR_PAUSE_MENU_HPP_
#define RR_PAUSE_MENU_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <iostream>
#include <string>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include "OpenGL3_Context.hpp"
#include "Shader.hpp"

namespace def
{

class RR_Pause_Menu
{
	def::Shader _pm_shaders;
	sf::Image _pm_image;
	std::unique_ptr< GLuint[] > _pm_vao_buffer;
	std::unique_ptr< GLint[] > _pm_mvp_loc;
	std::unique_ptr< GLint[] > _pm_tex_loc;
	std::unique_ptr< GLuint[] > _pm_tex_buffer;
	glm::mat4 _pm_mvp;
	std::unique_ptr< GLuint[] > _pm_vertices_buffer;
	std::unique_ptr< GLfloat[] > _pm_vertices;
	std::unique_ptr< GLuint[] > _pm_colors_buffer;
	std::unique_ptr< GLfloat[] > _pm_colors;
	std::unique_ptr< GLuint[] > _pm_tex_coords_buffer;
	std::unique_ptr< GLfloat[] > _pm_tex_coords;
	std::unique_ptr< GLuint[] > _pm_normals_buffer;
	std::unique_ptr< GLfloat[] > _pm_normals;
	bool _pm_first_render_error;

public:
	void pm_init(int resolution_width, int resolution_height); ///< Initialize the pause menu.
	void pm_handle_events(float& time);	///< Handle the pause menu events.
	void pm_display();	///< Display the pause menu.

	RR_Pause_Menu(def::OpenGL3_Context& opengl3_context);
	virtual ~RR_Pause_Menu();
};

}

#endif /* RR_PAUSE_MENU_HPP_ */
