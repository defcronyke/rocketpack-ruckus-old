/*	Copyright © 2012 Jeremy Carter
 * --------------------------------
 *  This file is part of Rocketpack Ruckus.
 *
 *  Rocketpack Ruckus is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License version 3,
 *  as published by the Free Software Foundation.
 *
 *  Rocketpack Ruckus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
 * --------------------------------
 *
 * Shader.hpp
 *
 *  Created on: 2012-06-19
 *      Author: Jeremy Carter
 */

#ifndef SHADER_HPP_
#define SHADER_HPP_

// get config from CMake
#include "rocketpack-ruckus-config.hpp"

#include <string>
#include <fstream>

#if ( (defined(__MACH__)) && (defined(__APPLE__)) )
	#include <stdlib.h>
	#include <OpenGL/gl.h>
	#include <GLUT/glut.h>
	#include <OpenGL/glext.h>
#else
	#include <GL/glew.h>
	#include <fstream>
#endif

#include "File.hpp"

namespace def
{

/// a GLSL shader helper class
/** Load shaders, bind and unbind them. */
class Shader
{
	unsigned int _shader_id;	///< the shader program handle
	unsigned int _shader_vp;	///< the vertex shader handle
	unsigned int _shader_fp;	///< the fragment shader handle
	File _vertex_shader_file;	///< the vertex shader source file
	File _fragment_shader_file;	///< the fragment shader source file

public:
	/// load vertex and fragment shaders from disk
	Shader(const std::string& vertex_shader_filename, const std::string& fragment_shader_filename);
	Shader();
	virtual ~Shader();

	/// compile and link shaders (called by the constructor)
	void init(const std::string& vertex_shader_filename, const std::string& fragment_shader_filename);
	void bind();	///< bind shaders with glUseProgram()
	void unbind();	///< unbind shaders with glUseProgram()
	unsigned int id();	///< get the shader id handle
};

}

#endif /* SHADER_HPP_ */
