level = {
	objects = {
		
		{ id = "a_guy",				-- the filenames for all this object's
		  first_name = "A Guy" },	-- resources need to be: a_guy.extension
		
		{ id = "a_girl",
		  first_name = "A Girl" }
		  
	},
	
	main_loop = function()
		print("Lua main_loop called")
	end
}
