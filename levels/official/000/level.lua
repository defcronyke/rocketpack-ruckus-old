level = {
	players = {

		{ name = "",
		  pos = { 0, 0, 0 },
		  status = { },
		  health = 100,
		  items = { rocketpack = { fuel = 100,
								   fuel_efficiency = 1,
								   max_speed = 10 } } }
	},

	objects = {
		
		{ id = "wall1",
		  pos = { 0, 0, -29.7 } },
		
		{ id = "wall2",
		  pos = { -29.7, 0, 0 },
  		  rot = 0 },
		  
		{ id = "wall3",
		  pos = { 29.7, 0, 0 } },
		  
		{ id = "wall4",
		  pos = { 0, 15, 29.6 },
		  direction = 'up',
		  speed = 1 },
		  
		{ id = "carpet1",
		  pos = { 0, 0, 0 } },
		  
		{ id = "cube1",
		  pos = { 0, 2, 25 },
		  speed = 0.1,
		  rot = 0,
		  rot_speed = 4 },
		  
		{ id = "cube2",
		  pos = { 15, 3, -20 },
		  speed = 0.1,
		  rot = 0,
		  rot_speed = 0.5,
		  direction = 1,
		  max_height = 5,
		  min_height = 0 },
		  
		{ id = "head1",
		  pos = { 0, 0, 0 },
		  rot = 0,
		  rot_speed = 8 },
		
		{ id = "snowman1",
		  pos = { 10, 0, -40 } }
		
	},
	
	main_loop = function()
		
		-- make wall4 move up and down
		wall4 = level.objects[4]
		
		if wall4.direction == 'up' then
			if wall4.pos[2] < 50 then
				wall4.pos[2] = wall4.pos[2] + wall4.speed
			else
				wall4.direction = 'down'
			end
		elseif wall4.direction == 'down' then
			if wall4.pos[2] > 0 then
				wall4.pos[2] = wall4.pos[2] - wall4.speed
			else
				wall4.direction = 'up'
			end
		end
		
		-- make head1 move around randomly
		head1 = level.objects[8]
		math.randomseed(os.time())
		plus_or_minus = math.random(0,1)
		if plus_or_minus == 0 then -- if plus
			head1.pos[1] = head1.pos[1] + (math.random(0,1) / 8)
			head1.pos[2] = head1.pos[2] + (math.random(0,1) / 8)
			head1.pos[3] = head1.pos[3] + (math.random(0,1) / 8)
		elseif plus_or_minus == 1 then -- if minus
			head1.pos[1] = head1.pos[1] - (math.random(0,1) / 8)
			head1.pos[2] = head1.pos[2] - (math.random(0,1) / 8)
			head1.pos[3] = head1.pos[3] - (math.random(0,1) / 8)
		end
		if head1.rot < 0 then
			head1.rot = 360
		end
		head1.rot = head1.rot - head1.rot_speed
		
		-- make slide around the edge of the room
		cube1 = level.objects[6]
		
		if cube1.pos[3] >= 25 then
			cube1.pos[1] = cube1.pos[1] + cube1.speed
		end
		
		if cube1.pos[1] >= 25 then
			cube1.pos[3] = cube1.pos[3] - cube1.speed
		end
		
		if cube1.pos[3] <= -25 then
			cube1.pos[1] = cube1.pos[1] - cube1.speed
		end
		
		if cube1.pos[1] <= -25 then
			cube1.pos[3] = cube1.pos[3] + cube1.speed
		end
		
		-- make cube2 bob and spin
		cube2 = level.objects[7]
		
		if cube2.pos[2] >= cube2.max_height then
			cube2.direction = 2
		elseif cube2.pos[2] <= cube2.min_height then
			cube2.direction = 1
		end
		
		if cube2.direction == 1 then
			cube2.pos[2] = cube2.pos[2] + cube2.speed
		elseif cube2.direction == 2 then
			cube2.pos[2] = cube2.pos[2] - cube2.speed
		end
		
		if cube2.rot > 360 then
			cube2.rot = 0
		end
		cube2.rot = cube2.rot + cube2.rot_speed

	end
}

