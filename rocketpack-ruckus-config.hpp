// *	Copyright © 2012 Jeremy Carter
// * --------------------------------
// *  This file is part of Rocketpack Ruckus.
// *
// *  Rocketpack Ruckus is free software: you can redistribute it and/or
// *  modify it under the terms of the GNU General Public License version 3,
// *  as published by the Free Software Foundation.
// *
// *  Rocketpack Ruckus is distributed in the hope that it will be useful,
// *  but WITHOUT ANY WARRANTY; without even the implied warranty of
// *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *  GNU General Public License for more details.
// *
// *  You should have received a copy of the GNU General Public License
// *  along with Rocketpack Ruckus.  If not, see <http://www.gnu.org/licenses/>.
// * --------------------------------

// This file is used to pass info from CMake
// to the source code for the project.

#define rocketpack_ruckus_VERSION_MAJOR 0
#define rocketpack_ruckus_VERSION_MINOR 0

#define DATADIR "/home/bakeonandham/eclipse_workspace/rocketpack-ruckus"
#define PROJECTNAME "rocketpack-ruckus"
