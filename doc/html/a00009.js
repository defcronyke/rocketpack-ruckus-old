var a00009 =
[
    [ "OpenGL3_Context", "a00009.html#a422e327f9faafe1ff5a23a7dfe9b1128", null ],
    [ "~OpenGL3_Context", "a00009.html#aa440831accd7e04cf1ccd634bf0acb9f", null ],
    [ "_context_settings", "a00009.html#af097584cc5343eb64869d227019ea015", null ],
    [ "_model_matrix", "a00009.html#a87b8838a5512621e5819b11f77d670bd", null ],
    [ "_projection_matrix", "a00009.html#ace2fc89b8b853def771ef1f794c81a38", null ],
    [ "_view_matrix", "a00009.html#a3834a913f5496a58318c47c03a751c81", null ],
    [ "_window_bpp", "a00009.html#a1a9cf2b5dd2ea81069ee5ceb1c872674", null ],
    [ "_window_title", "a00009.html#ab7d2133a45831ea4402d005b9a6221eb", null ],
    [ "window", "a00009.html#ac5b95917279bd6df66e3b6a76fd855f3", null ],
    [ "window_height", "a00009.html#a6dc92168b20c0b6036d9b5ca2c0113b2", null ],
    [ "window_width", "a00009.html#ad763bd03350369faf5d67868042e1a04", null ]
];