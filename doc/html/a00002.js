var a00002 =
[
    [ "Controls", "a00002.html#a465e039da6a046f3cb92c9b8afb153b8", null ],
    [ "~Controls", "a00002.html#aa3fc616e9716fa6a2f518d4d9ad5d80b", null ],
    [ "_map_controls", "a00002.html#a65f1f8b8792a87e7acd57f7fa67119b4", null ],
    [ "_controls_conf", "a00002.html#aec51a3080d055e6ebfbf54bcdba71a43", null ],
    [ "controls_conf_map", "a00002.html#ab5215bf2e3e0258195beced58bf5c691", null ],
    [ "Down", "a00002.html#a377a2aff15c77a112cfd4ffd64415a7f", null ],
    [ "Jump", "a00002.html#a591597079328acefddb0023724bb2898", null ],
    [ "Left", "a00002.html#a343de1f72926cbf0ecfa7e2bc5e61574", null ],
    [ "Pause", "a00002.html#a5d9caebbef926dbddef79adfe3a71624", null ],
    [ "Right", "a00002.html#a1b8a57d3f1da546c06fdee341e992413", null ],
    [ "Rocketpack", "a00002.html#a4fc8bab60c7fe9b12fb28604f04f4240", null ],
    [ "Up", "a00002.html#afd66468ee9bc189f843d29ccdd52a5b1", null ]
];