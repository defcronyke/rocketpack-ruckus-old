var hierarchy =
[
    [ "def::Assimp_Resource", "a00001.html", [
      [ "def::RR_Object", "a00011.html", null ],
      [ "def::RR_Player", "a00013.html", null ]
    ] ],
    [ "def::Controls", "a00002.html", null ],
    [ "def::exception_Assimp", "a00003.html", null ],
    [ "def::exception_File", "a00004.html", null ],
    [ "def::exception_OpenGL_GLEW", "a00005.html", null ],
    [ "def::exception_SFML", "a00006.html", null ],
    [ "def::File", "a00007.html", null ],
    [ "def::Lua_Level_Loader", "a00008.html", null ],
    [ "def::OpenGL3_Context", "a00009.html", null ],
    [ "def::RR_HUD", "a00010.html", null ],
    [ "def::RR_Pause_Menu", "a00012.html", null ],
    [ "def::Shader", "a00014.html", null ],
    [ "def::Video", "a00015.html", null ]
];